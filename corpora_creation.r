library(tidyverse)
library(quanteda)
library(readxl)
library(ggthemes)
library(tm)
library(topicmodels)
library(stm)
library(lda)
library(SnowballC)
library(jiebaR)
library(stopwords)


# Load data ---------------------------------------------------------------

prov1 <- read_csv("543.csv")
names(prov1) 
  #' This is a web-scraped data (Qixuan Yang, 2018/2019). The full content is listed in zoom_1, 
  #' and the domain is listed in domainID and domainName

prov1[67, c("zoom_1", "domainId", "domainName")] # Example

  #' Since it is Chinese language, it is required that the words shall be splitted. Here, we use
  #' jiebaR to split the words. The reason for using this package instead of a whole-sale solution
  #' provided by many corpus-generating packages is that we are better to tune what kind of mode of split,
  #' what kind of stopwords we may exclude etc.



# Split characters into dictionary mode -----------------------------------
# Using jiebaR package

zh_dict_gen <- function(type, vector, stopsource){
  wk <- worker(type = type, bylines = T) # type: mix, mp, hmm, full, query, tag, simhash, and keywords
  
  dict <- filter_segment(wk[vector], stopwords::stopwords('zh', source = stopsource)) # stopwords-iso, misc
  dict_list <- unique(unlist(dict))
  dict_list <- as.list(dict_list)
  names(dict_list) <- dict_list
  
  return(list(dict_list, dict))
}

prov1_dict_all <- zh_dict_gen("mix", prov1$zoom_1, "misc")
prov1_dict <- prov1_dict_all[[1]]
length(prov1_dict)

#intvl <- split(1:length(prov1_dict), 1:100) 
#prov1_dict_temp <- sapply(intvl, function(x) dictionary(prov1_dict[x]))
#prov1_dict_final <- prov1_dict_temp[[1]]

#for(i in 2:length(prov1_dict_temp)){
#  prov1_dict_final <- c(prov1_dict_final, prov1_dict_temp[[i]])
#  cat(i, "done \n")
#}

# Create text corpora -----------------------------------------------------

prov1_text <- lapply(prov1_dict_all[[2]], function(x) str_c(x, collapse = " ")) # Tokenizing

prov1_corpus <- Corpus(VectorSource(prov1_text))
prov1_dtm <- DocumentTermMatrix(prov1_corpus, control = list(wordLengths = c(1, Inf)))
inspect(prov1_dtm)

library(tidytext)
prov1_td <- tidy(prov1_dtm)

rm_digit <- which(str_detect(prov1_td$term, "\\d+|[a-zA-Z]+|﹉|ー|㎡") == T)
  # Remove unneccessary tokens

prov1_dfm <- prov1_td[-rm_digit, ] %>% cast_dfm(document, term, count) # Convert to a quanteda dfm object
prov1_stm <- convert(prov1_dfm, to = "stm")
head(prov1_stm$vocab) # Snippet of the vocab, shall be identical as prov1_dict

# Output ------------------------------------------------------------------

saveRDS(prov1_dict_final, "corpora/prov1_dict.rds")
saveRDS(prov1_dfm, "corpora/prov1_dfm.rds")
saveRDS(prov1_stm, "corpora/prov1_stm.rds")




















