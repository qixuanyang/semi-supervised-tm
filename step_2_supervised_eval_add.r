library(plyr)
library(tidyverse)
library(foreign)
library(quanteda)
library(readxl)
library(tm)
library(topicmodels)
library(stm)
library(SnowballC)
library(psych)
library(tidytext)
library(showtext)
library(googleLanguageR)

font_add("fangsong", regular = "AdobeFangsongStd-Regular.otf") # For showing Chinese texts


# Load data ---------------------------------------------------------------

label_info <- read_csv("step_1/label_set_info.csv") # one-to-one relationship between labels and their indicies

prov1 <- read_csv("543.csv")
other_ind <- which(prov1$domainName == "其他")
prov1_dfm <- read_rds("corpora/prov1_dfm.rds")
other_dfm <- prov1_dfm[other_ind, ]
other_dtm <- convert(other_dfm, to = "stm") # Only 12826 vocab left

wc <- tidy(other_dfm) %>% group_by(term) %>% dplyr::summarise(count = sum(count)) # Word count
wc <- inner_join(wc, data_frame(term = other_dtm$vocab, 
                                id = c(1:length(other_dtm$vocab))))

all_pred <- read_csv("step_2/all_pred_with_entropy.csv")


# Train STM on Test Set ---------------------------------------------------


other_mod <- manyTopics(other_dtm$documents,
                        other_dtm$vocab, K = c(20:25), 
                        runs = 50, frexw = 0.5, M = 20,
                        seed = 12345, control = list(alpha = 0.8))

saveRDS(other_mod, "test_set_stm/test_set_topic_model.rds")
other_mod <- read_rds("test_set_stm/test_set_topic_model.rds")

sapply(other_mod$semcoh, psych::describe) # Summary statistics of the semantic coherence score
sapply(other_mod$exclusivity, psych::describe) # Summary statistics of the semantic coherence score


# Exporting posterior and frex --------------------------------------------

lapply(other_mod$out, 
       function(mod) write_csv(as.data.frame(t(sapply(thetaPosterior(mod, nsims = 1000, type = "Global"),colMeans))),
                               paste0("test_set_stm/posterior/", "k",mod$settings$dim$K, "_", "a",
                                      str_replace(mod$settings$init$alpha, "[.]","_"),".csv")))

frex_table <- function(model, weight, wc){
  num_out <- calcfrex(model$beta$logbeta[[1]], w = weight, wordcounts = wc$count)
  frex_table <- plyr::mapvalues(num_out, wc$id, wc$term)
  return(frex_table)
}

lapply(other_mod$out, 
       function(mod) write_csv(as.data.frame(head(frex_table(mod, 0.5, wc), 20)),
                               paste0("test_set_stm/frex/", "k",mod$settings$dim$K, "_", "a",
                                      mod$settings$init$alpha,".csv")))



# Multivariate Regression -------------------------------------------------

library(nnet)

feature <- read_csv("test_set_stm/posterior/k25_a0_8.csv")*100
y <- all_pred$k25_a0_8_all.h5
y <- as.factor(y)

dat <- cbind(feature, y)

multinom_fit <- nnet(feature, y, size = 0, 
                     weights = rep(1, nrow(dat)), censored = T, maxit = 1000)

multinom_sum <- summary(multinom_fit)
multinom_sum$cf_low <- multinom_sum$coefficients - 1.96*multinom_sum$standard.errors
multinom_sum$cf_high <- multinom_sum$coefficients + 1.96*multinom_sum$standard.errors

i = 1
plot(x = multinom_sum$coefficients[i,-1], y = c(1:25), pch = 19, xlim = c(-0.7, 0.7),
     main = label_info$label[i+1])

sapply(1:25, function(x) lines(c(multinom_sum$cf_high[i,(x+1)],multinom_sum$cf_low[i,(x+1)]), c(x,x)))

abline(v= 0)










