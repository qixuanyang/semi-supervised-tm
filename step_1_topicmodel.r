library(plyr)
library(tidyverse)
library(foreign)
library(quanteda)
library(readxl)
library(tm)
library(topicmodels)
library(stm)
library(SnowballC)
library(psych)
library(tidytext)

library(showtext)
font_add("fangsong", regular = "AdobeFangsongStd-Regular.otf") # For showing Chinese characters. Users may choose other fonts

# Load constructs ---------------------------------------------------------

prov1_dtm <- read_rds("corpora/prov1_stm.rds")
prov1 <- read_csv("543.csv")
table(prov1$domainId, prov1$domainName) %>% dim() # 14 Topics
prov1_dfm <- read_rds("corpora/prov1_dfm.rds")

#' Create word count table: term, count, id
wc <- tidy(prov1_dfm) %>% group_by(term) %>% summarise(count = sum(count)) # Word count
wc <- inner_join(wc, data_frame(term = prov1_dtm$vocab, 
                                id = c(1:length(prov1_dtm$vocab)))) # Indexing according to stm corpora

#' Inspecting the label set, and translating it into English
prov1$domainName %>% unique()
prov1$domainName_en <- as.factor(prov1$domainName)
levels(prov1$domainName_en) <- c("Agriculture", "Transportation", "Business",
                                 "Other", "Medicine", "Urban_Planning",
                                 "Employment", "Bureaucracy", "Education",
                                 "Culture_Entertainment", "Tourism", 
                                 "Public_Security", "Environment", "Finance")

# Create and select models ------------------------------------------------


#' Proposed models with different k and alpha. 

##' K-search, assumed mixed-membership (Validation set = 10%)

k_search_v1 <- searchK(prov1_dtm$documents,
                    prov1_dtm$vocab, K = c(12:30),
                    N = floor(0.1*length(prov1_dtm$documents)),
                    control = list(alpha = 2))
saveRDS(k_search_v1, "step_1/k_search_v1.rds")

k_search_v1 <- readRDS("step_1/k_search_v1.rds")

plot.searchK(k_search_v1)

feasible_k_v1 <- c(17, 20, 23, 25, 27, 30)

##' K-search, assumed single-membership (Validation set = 10%)

k_search_v2 <- searchK(prov1_dtm$documents,
                       prov1_dtm$vocab, K = c(12:30),
                       N = floor(0.1*length(prov1_dtm$documents)),
                       control = list(alpha = 0.8))
saveRDS(k_search_v2, "step_1/k_search_v2.rds")

k_search_v2 <- readRDS("step_1/k_search_v2.rds")

plot.searchK(k_search_v2)

feasible_k_v2 <- c(17, 18, 20, 23, 25, 28, 30)

##' Candidate models (maximum iterations up to 100 - Model converge at a level 
##' better than a 1e-04 change)
##' For replication purpose, please use the same seeds as shown here.

candidate_models_v1 <- manyTopics(prov1_dtm$documents,
                                  prov1_dtm$vocab, K = feasible_k_v1, # Different specification of k
                                  runs = 50, # 50 runs for each parameter-pair
                                  frexw = 0.5, # The weight for exclusivity. 0.5 indicates a balance
                                  M = 20, # 20 words for frex
                                  seed = 12345,
                                  control = list(alpha = 2))

saveRDS(candidate_models_v1, "step_1/candidate_models_v1.rds")


candidate_models_v1 <- read_rds("step_1/candidate_models_v1.rds")
sapply(candidate_models_v1$semcoh, psych::describe) # Summary statistics of the semantic coherence score


candidate_models_v2 <- manyTopics(prov1_dtm$documents,
                                  prov1_dtm$vocab, K = feasible_k_v2,
                                  runs = 50, frexw = 0.5, M = 20,
                                  seed = 6789,
                                  control = list(alpha = 0.8))
saveRDS(candidate_models_v2, "step_1/candidate_models_v2.rds")


candidate_models_v2 <- read_rds("step_1/candidate_models_v2.rds")
sapply(candidate_models_v2$semcoh, psych::describe) # Summary statistics of the semantic coherence score


# Exporting posteriors and frex -------------------------------------------

#' Posterior (Point Estimate)

lapply(candidate_models_v2$out, 
       function(mod) write_csv(as.data.frame(t(sapply(thetaPosterior(mod, nsims = 1000, type = "Global"),colMeans))),
                               paste0("step_1/posterior/", "k",mod$settings$dim$K, "_", "a",
                                      str_replace(mod$settings$init$alpha, "[.]","_"),".csv")))

lapply(candidate_models_v1$out, 
       function(mod) write_csv(as.data.frame(t(sapply(thetaPosterior(mod, nsims = 1000, type = "Global"),colMeans))),
                               paste0("step_1/posterior/", "k",mod$settings$dim$K, "_", "a",
                                      mod$settings$init$alpha,".csv")))

#' Frex (exclusivity == 0.5) with top 20 words

frex_table <- function(model, weight, wc){
  num_out <- calcfrex(model$beta$logbeta[[1]], w = weight, wordcounts = wc$count) # calcfrex for frex calculation
  frex_table <- plyr::mapvalues(num_out, wc$id, wc$term) # replacing the word id with terms
  return(frex_table)
}

lapply(candidate_models_v1$out, 
       function(mod) write_csv(as.data.frame(head(frex_table(mod, 0.5, wc), 20)),
                               paste0("step_1/frex/", "k",mod$settings$dim$K, "_", "a",
                                      mod$settings$init$alpha,".csv")))

lapply(candidate_models_v2$out, 
       function(mod) write_csv(as.data.frame(head(frex_table(mod, 0.5, wc), 20)),
                               paste0("step_1/frex/", "k",mod$settings$dim$K, "_", "a",
                                      str_replace(mod$settings$init$alpha, "[.]","_"),".csv")))

#' Label set (Marking "Other" as NA, since the texts with this label will be in the test set)

levels(prov1$domainName_en)[4] # Other
levels(prov1$domainName_en)[4] <- NA
table(prov1$domainName_en, prov1$domainName, exclude = F) # Check

levels(prov1$domainName_en)

label_set <- data_frame(label = prov1$domainName_en,
                        label_num = as.numeric(prov1$domainName_en)-1)

write_csv(label_set, "step_1/label_set.csv")

write_csv(label_set %>% group_by(label, label_num) %>% summarise(count = n()),
          "step_1/label_set_info.csv") # Table for the one-to-one relationship between labels and ther indices



#' Additional covariate - Year (as character, not numeric)
convert_date <- function(x){
  
  # The date was coded in a 10-digit form
  # This function is designed to translate it into
  # yyyy-mm-dd, and then export only the year (yyyy)

  date <- floor(x/60/60/24)
  date <- as.Date("1970-1-1") + date
  year <- str_match(date, "\\d{4}")
  return(year)
}

write_csv(data_frame(year = sapply(prov1$dateline, convert_date)),
          "step_1/covariate_set.csv")




