library(plyr)
library(tidyverse)
library(foreign)
library(quanteda) # Tidy
library(readxl)
library(tm)
library(topicmodels)
library(stm)
library(SnowballC)
library(psych)
library(tidytext)
library(text2vec)
library(showtext)
font_add("fangsong", regular = "AdobeFangsongStd-Regular.otf") # For showing Chinese characters. Users may choose other fonts


# Load data and constructs ------------------------------------------------

prov1_dtm <- read_rds("corpora/prov1_stm.rds")
prov1 <- read_csv("543.csv")
table(prov1$domainId, prov1$domainName) %>% dim() # 14 Topics
prov1_dfm <- read_rds("corpora/prov1_dfm.rds")

#' Create word count table: term, count, id
wc <- tidy(prov1_dfm) %>% group_by(term) %>% summarise(count = sum(count)) # Word count
wc <- inner_join(wc, data_frame(term = prov1_dtm$vocab, 
                                id = c(1:length(prov1_dtm$vocab)))) # Indexing according to stm corpora

#' Inspecting the label set, and translating it into English
prov1$domainName %>% unique()
prov1$domainName_en <- as.factor(prov1$domainName)
levels(prov1$domainName_en) <- c("Agriculture", "Transportation", "Business",
                                 "Other", "Medicine", "Urban_Planning",
                                 "Employment", "Bureaucracy", "Education",
                                 "Culture_Entertainment", "Tourism", 
                                 "Public_Security", "Environment", "Finance")

#' Previously trained model in step 1
candidate_models_v1 <- read_rds("step_1/candidate_models_v1.rds")
candidate_models_v2 <- read_rds("step_1/candidate_models_v2.rds")


# Semantic scoring --------------------------------------------------------

sapply(candidate_models_v1$semcoh, psych::describe)
sapply(candidate_models_v2$semcoh, psych::describe)



# FREX Similarities -------------------------------------------------------
#' Goal: Transform the frex into vectors, and compare them with each other

#' Loading FREX table while transform the feature columns into vectors (in loop)

frex_dat <- data_frame(model = NA, frex = NA, v = NA) # Container

for(i in 1:length(list.files("step_1/frex"))){
  frex <- read_csv(paste0("step_1/frex/", list.files("step_1/frex")[i]))
  temp <- data.frame(model = list.files("step_1/frex")[i],
             frex = sapply(frex, function(x) str_c(x, collapse = ", ")),
             v = seq(1, ncol(frex) ) )
  frex_dat <- bind_rows(frex_dat, temp)
}

frex_dat <- frex_dat[-1,]
frex_dat$id <- 1:nrow(frex_dat)

#' Tokenizing for later similarity check (word2vec package)

it <- itoken(frex_dat$frex %>% str_remove_all(","), progressbar = FALSE)
v <- create_vocabulary(it)
vectorizer <- vocab_vectorizer(v)
frex_dtm <- create_dtm(it, vectorizer)
rm(it, v, vectorizer)

#' JAARD and COSINE similaries

jaard <- sim2(frex_dtm, frex_dtm, method = "jaccard", norm = "none")
cosine <- sim2(frex_dtm, frex_dtm, method = "cosine", norm = "l2")

  #' Using JAARD similarity. Defining 0.5 - 1 as high similarityy

frex_high_sim <- as_data_frame(cbind(which(as.matrix(jaard) < 1 & as.matrix(jaard) >= 0.5, arr.ind = T), which(as.matrix(jaard) < 1 & as.matrix(jaard) >= 0.5, arr.ind = T)))

frex_high_sim <- frex_high_sim %>% filter(row > col) # Removing duplicates

frex_high_sim$row <- plyr::mapvalues(frex_high_sim$row, frex_dat$id, frex_dat$frex) # Replacing indices with feature vectors
frex_high_sim$col <- plyr::mapvalues(frex_high_sim$col, frex_dat$id, frex_dat$frex)
frex_high_sim$row1 <- plyr::mapvalues(frex_high_sim$row1, frex_dat$id, frex_dat$model) # Replacing indices with topic names
frex_high_sim$col1 <- plyr::mapvalues(frex_high_sim$col1, frex_dat$id, frex_dat$model)

table(c(frex_high_sim$row1, frex_high_sim$col1)) # Simply count the models that appear the most
  #' Following four constructs are quite good according to jaard
  #' (i.e., with strong matching pattern with other constructs)
  #' k20_a0_8
  #' k25_a0_8
  #' k30_a0_8
  #' k17_a2
  #' 
  #' It seems to be that the data generating process is single-peaked.


# Sample document visualization -------------------------------------------
#' Since it is not a systematic method to determine the quality of 
#' topical attributes, this visualization just presents some intuitive
#' understanding. Suitable for a presentation / sample explanation

mod <- candidate_models_v2$out[[1]] # Example model

  #' 1. Generating frex for topical features labeling

frex_table <- function(model, weight, wc){
  # Defining the frex_table function. For details, please refer to the
  # same function in step_1_topicmodel.r

  num_out <- calcfrex(model$beta$logbeta[[1]], w = weight, wordcounts = wc$count)
  frex_table <- plyr::mapvalues(num_out, wc$id, wc$term)
  return(frex_table)
}

mod_frex <- frex_table(mod, 0.5, wc) # Modest exclusivity

k <- mod$settings$dim$K
alpha <- mod$settings$init$alpha
nsims <- 1000

  #' 2. Generating the posterior distribution for each document using simulations

mod_posterior_point <- sapply(thetaPosterior(mod, nsims = nsims, type = "Global"),
                              colMeans)

posterior <- thetaPosterior(mod, nsims = nsims, type = "Global")

cf <- function(doc, topic){
  # Generating the 95% interval
  avg <- mean(posterior[[doc]][,topic])
  se <- (sd(posterior[[doc]][,topic])/sqrt(nsims))
  upper <- avg + 1.96*se
  lower <- max(0, avg - 1.96*se)
  return(c(avg, upper, lower))
}

mod_posterior <- lapply(1:mod$settings$dim$N, 
                        function(doc) sapply(1:k, function(x) cf(doc,x)))
rm(posterior)

  #' 3. Graph specification

ind_graph <- function(mod_posterior, doc, threshold){

  #' 3.1 Plotting the point estimate of the posterior

  plot(mod_posterior[[doc]][1,], type = "p", ylim = c(0,1), xlim = c(1,k+1),
       pch = 19,
       main = paste("Document", doc, "-", prov1$domainName[doc]),
       ylab = "Probability",
       xlab = paste("Topic model with k =", k, "alpha =", alpha))
  
  #' 3.2 Upper and lower bounds of the posterior
  
  sapply(1:k, function(x) lines(c(x,x), c(mod_posterior[[doc]][2,x], 
                                          mod_posterior[[doc]][3,x])))
  
  #' 3.3 Adding the texts of topical features, whose probability is higher than a 
  #' predefined threshold

  mod_posterior_point <- sapply(mod_posterior, function(x) x[1,])
  
  topics_d <- which(mod_posterior_point[,doc] > threshold)
  
  sapply(topics_d, function(x) text(x+1, mod_posterior[[doc]][1,x],
                                    str_c(mod_frex[1:5,x], collapse = "\n")))
}

  #' 3.4 Showing the document

textgraph <- function(doc){
  plot(c(0,10), c(0,10), ann = F, bty = 'n', type = 'n', xaxt = 'n', yaxt = 'n')
  text(x = 5, y = 5, str_wrap(prov1$zoom_1[doc], width = 40), cex = 0.8, col = "black")
}

  #' Sample document

doc <- 1770
threshold <- 0.15

cairo_pdf("test.pdf")
showtext_begin()
par(mfrow = c(2,1))
ind_graph(mod_posterior, doc, threshold)
textgraph(doc)
par(mfrow = c(1,1))
dev.off()

# Document-based comparison -----------------------------------------------
#' Extending the previous document posterior plots for different topics.
#' Holding a document constant and see how it gets classified in different topic models
#' Comment of the functions sees the last section

doc <- 1770
threshold <- 0.15


cairo_pdf("sample_text.pdf", width = 17, height = 9)
showtext_begin()
par(mfrow = c(2,4))

for(i in 1:length(candidate_models_v2$out)){
  mod <- candidate_models_v2$out[[i]]
  mod_frex <- frex_table(mod, 0.5, wc)
  
  k <- mod$settings$dim$K
  alpha <- mod$settings$init$alpha
  nsims <- 1000
  
  posterior <- thetaPosterior(mod, nsims = nsims, type = "Global")
  mod_posterior <- lapply(1:mod$settings$dim$N, 
                          function(doc) sapply(1:k, function(x) cf(doc,x)))
  rm(posterior)
  
  ind_graph(mod_posterior, doc, threshold)
  
}

textgraph(doc)

for(i in 1:length(candidate_models_v1$out)){
  mod <- candidate_models_v1$out[[i]]
  mod_frex <- frex_table(mod, 0.5, wc)
  
  k <- mod$settings$dim$K
  alpha <- mod$settings$init$alpha
  nsims <- 1000
  
  posterior <- thetaPosterior(mod, nsims = nsims, type = "Global")
  mod_posterior <- lapply(1:mod$settings$dim$N, 
                          function(doc) sapply(1:k, function(x) cf(doc,x)))
  rm(posterior)
  
  ind_graph(mod_posterior, doc, threshold)
  
}
textgraph(doc)
par(mfrow = c(1,1))
dev.off()


# Average probability of topical attributes given a label -----------------
#' Idea: First, choosing the document with a particular label in domainName (label-specific subset)
#' Then, compute the average probabilities of all topical features in that label-specific subset
#' Codes are similar to the ones in the section of "sample document visualization", except for
#' the generation of probabilities

label_set <- levels(prov1$domainName_en)
label_set <- label_set[-4] # Exclude "Other" for now
candidate_models_out <- c(candidate_models_v1$out, candidate_models_v2$out)

for(i in 1:length(candidate_models_out)){ # For each candidate model
  mod <- candidate_models_out[[i]]
  mod_frex <- frex_table(mod, 0.5, wc)
  
  k <- mod$settings$dim$K
  alpha <- mod$settings$init$alpha
  nsims <- 1000
  
  #' Calculating the posterior
  mod_posterior_point <- sapply(thetaPosterior(mod, nsims = nsims, type = "Global"), colMeans)
  
  #' Setting threshold
  threshold <- 0.1
  
  pdf(paste0("step_1/avg_prob/","k",k, "_", "a",str_replace(alpha, "[.]", "_"),".pdf"), width = 17, height = 9)
  
  par(mfrow = c(2,4))
  
  for(i in 1:length(label_set)){ # For each topical feature in each candidate model
    prov1_sub_ind <- which(prov1$domainName_en == label_set[i])
    
    #' Computing the average posterior among the label-specific subset

    mod_posterior_sub <- mod_posterior_point[,prov1_sub_ind]
    mod_posterior_sub <- rowMeans(mod_posterior_sub)
    
    showtext_begin()
    
    plot(mod_posterior_sub, type = "p", ylim = c(0,1), xlim = c(1,k+1),
         pch = 19,
         main = paste("Label - ", label_set[i]),
         ylab = "Probability",
         xlab = paste("Topic model with k =", k, "alpha =", alpha, "(Threshold for showing = ", threshold, ")"))
    
    
    topics_d <- which(mod_posterior_sub > threshold)
    
    sapply(topics_d, function(x) text(x+1, mod_posterior_sub[x],
                                      str_c(mod_frex[1:5,x], collapse = "\n")))
  }
  
  par(mfrow = c(1,1))
  
  dev.off()
}


# Avg prob english version ------------------------------------------------
# Using Google Translate API (Free version, with built-in model). For the
# authentification file in .json format, please consult the API service
# Codes comments see the last section.

library(googleLanguageR)
gl_auth("../googlcl.json")

for(i in 1:length(candidate_models_out)){
  mod <- candidate_models_out[[i]]
  
  mod_frex <- frex_table(mod, 0.5, wc)
  mod_frex_reduced <- head(mod_frex, 10)
  
  # Translate into english #
  mod_frex_en <- sapply(c(1:ncol(mod_frex_reduced)), function(x) gl_translate(mod_frex_reduced[,x], target = "en", source = "zh-CN")$translatedText)
  # Done #
  
  k <- mod$settings$dim$K
  alpha <- mod$settings$init$alpha
  nsims <- 1000
  
  mod_posterior_point <- sapply(thetaPosterior(mod, nsims = nsims, type = "Global"), colMeans)
  
  threshold <- 0.1
  
  pdf(paste0("step_1/avg_prob_en/","k",k, "_", "a",str_replace(alpha, "[.]", "_"),".pdf"), width = 17, height = 9)
  
  par(mfrow = c(2,4))
  
  for(i in 1:length(label_set)){
    prov1_sub_ind <- which(prov1$domainName_en == label_set[i])
    
    mod_posterior_sub <- mod_posterior_point[,prov1_sub_ind]
    mod_posterior_sub <- rowMeans(mod_posterior_sub)
    
    showtext_begin()
    
    plot(mod_posterior_sub, type = "p", ylim = c(0,1), xlim = c(1,k+1),
         pch = 19,
         main = paste("Label - ", label_set[i]),
         ylab = "Probability",
         xlab = paste("Topic model with k =", k, "alpha =", alpha, "(Threshold for showing = ", threshold, ")"))
    
    
    topics_d <- which(mod_posterior_sub > threshold)
    
    sapply(topics_d, function(x) text(x+1, mod_posterior_sub[x],
                                      str_c(mod_frex_en[1:5,x], collapse = "\n")))
  }
  
  par(mfrow = c(1,1))
  
  dev.off()
}
