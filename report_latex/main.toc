\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Topical Features and Human-Generated Labels}{3}{section.2}
\contentsline {section}{\numberline {3}Inference from Topical Features}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Accuracy Metrics}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}SVM-Weights}{8}{subsection.3.2}
\contentsline {section}{\numberline {4}Prediction Analysis}{12}{section.4}
\contentsline {subsection}{\numberline {4.1}Distribution of Predicted Labels}{12}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Topical Features and Machine-Predicted Labels}{15}{subsection.4.2}
\contentsline {section}{\numberline {5}Discussion}{15}{section.5}
\contentsline {section}{References}{17}{section.5}
