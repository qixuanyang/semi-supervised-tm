\section{Introduction}

The semi-supervised architecture follows a simple idea: In the first step, we use the topic model (e.g., latent Dirichlet allocation) to derive the posterior distribution of the document topic-domain according to the topic-related texts, as well as the list of words clustered in each domain. These lists of word representations are thus the semantic predictors ($X_{topic}$) of our feature set ($Y$). Further, we add some other document-based predictors, if applicable and plausible, into the feature set. At the second stage, we use multi-class supervised learning to train the model and predict the labels of test data.

Two-stage semi-supervised learning, as we argue in the original paper,\footnote{Yang, Qixuan and Lukas Ringlage (2018). Semi-Supervised Learning of Topic Classification: German Parliamentary Inquiries as an Example. Unpublished Manuscript. University of Konstanz.} is more suitable for the context where the data is only partially labeled, and where the word features might not be identically or similarly distributed among test and training set. It suggests that this model might be more computational expensive since there are two rounds of training, but has weaker assumptions than the supervised topic model on the equal word feature distribution or parameter similarity. Figure \ref{fig:arch} \cite{ownwork} visually summarizes the structure.

\begin{figure}[H]
    \centering
    \input{arch1.tex}
    \caption{Semi-Supervised Learning (From Yang and Ringlage, 2018)}
    \label{fig:arch}
\end{figure}


As for the example of German Parliamentary Inquiries in the original paper, we received an overall 69.76\% validation accuracy in a 7-class classification task and 56.93\% in a 21-class scenario. More than the accuracy, the inferences from the supervised learning using Support Vector Machines (SVM) also reveal the data generating process of the human-labeling, as well as different partisan preferences. However, this context is favorable for the algorithm: First, it was a process of expert labeling with human-coders who are trained and knowledgable. Second, the generation of texts stems from politicians, who have presumably a more precise mindset when sending \textit{political} inquiries. It was unclear whether the proposed architecture may travel to a context whether \textit{both} of the data generating processes are less strict.

In this exercise, I use the proposed architecture to perform an illustrative task with political text data from China. The data were scrapped from the People's Daily Message Board\footnote{\url{http://liuyan.people.com.cn}} in September 2018. This platform is organized by the People's Daily, the mouthpiece of the Communist Party of China. Officials, usually Party Secretaries and Heads of Executive from provincial to county level are responsible for the reply. 

The texts submitted to this platform, as compared to the parliamentary inquiries, presumbly possess a less formal data-generating process in two regards: First, citizens might formulate their political concerns differently from the politicians, and the variance could be more substantial. Second, citizens have to choose a category for their texts - a highly subjective process without the possibility of inter-subjective verification during the human-coding. In other words, either the generation of the feature set ($X$) or the label set ($Y$) might be less formal.

Here, I use the sample data from the Hebei Province. Among 6000 observations, 4923 documents are classified with a substantive issue. The rest 1077 documents are classified as ``Other", which is an ambiguous category. Table \ref{tab:issue} describes the distribution. Here, we observe a strong representation of issues in terms of Agriculture and Urban Planning. Regarding the raw data, the "Other"-category makes up roughly 18\%, which is far from a random pattern. 

Two tasks are performed in this technical report: First, I examine the inference from the textual features to substantive labels. Second, I am interested in the prediction of the ``Other" category. Therefore, I split the data into training data that consist of texts with substantive labels, and test data with documents labeled as ``Other". 

\begin{table}[t]
\centering
\caption{Distribution of Substantive Labels}
\begin{tabular}{clrrr}
  \hline \hline
 No. & Label & Count & Raw Proportion & Proportion of Training Data \\ 
  \hline
  0 & Agriculture & 771 & 12.85 & 15.66 \\ 
  1 & Transportation & 329 & 5.48 & 6.68 \\ 
   2 & Business & 153 & 2.55 & 3.11 \\ 
   3 & Medicine & 287 & 4.78 & 5.83 \\ 
  4 & Urban Planning & 1565 & 26.08 & 31.79 \\ 
  5 & Employment & 354 & 5.90 & 7.19 \\ 
  6 & Bureaucracy & 333 & 5.55 & 6.76 \\ 
   7 & Education & 492 & 8.20 & 9.99 \\ 
   8 & Culture/Entertainment &  24 & 0.40 & 0.49 \\ 
  9 & Tourism &  50 & 0.83 & 1.02 \\ 
  10 & Public Security & 164 & 2.73 & 3.33 \\ 
  11 & Environment & 390 & 6.50 & 7.92 \\ 
  12 & Finance &  11 & 0.18 & 0.22 \\ 
  & Other & 1077 & 17.95 &  \\ 
   \hline \hline
\end{tabular}
\label{tab:issue}
\end{table}

This article will be organized as follows: First of all, I present the first inspection regarding the association between topical features and labels on average. Then, the results of the classification effort will be presented through two lenses: First, I discuss the accuracy in the training set, as well as the inference in terms of the SVM-weights. Second, the prediction effort will be investigated in a descriptive and analytic manner.

\section{Topical Features and Human-Generated Labels}
Prior to the unsupervised training, I tokenized the texts with a mixed-model in the \texttt{jiebaR} package in R \cite{jiebar}. A simple preprocessing regarding the removal of stopwords is conducted.\footnote{The \texttt{stopwords} package in R \cite{stopwords} uses the dictionary of the Baidu stopwords (\url{http://www.baiduguide.com/baidu-stopwords/}).} Since the first step of latent Dirichlet allocation \cite{blei2003latent} is the unsupervised learning, which means that we do not have information about an empirical distribution of topics. Therefore, I proposed 36 models with $k \in [12, 30], k \in \mathbb{Z}^{+}$ and $\alpha \in \{0.8, 2 \}$. In practice, I apply the Structural Topic Model developed by \citeA{roberts2014structural}, which can be viewed as an extension based on the LDA and proposes an innovative way to specify priors.

Voyaging through these different settings help understand whether the data are generated from a mixture-model (i.e., a text can often cover more than one topic) or a single-membership one. $\alpha$ in the Dirichlet allocation defines this property: When it is larger than $1$, a mixture-model is assumed, otherwise the single-membership. Second, since there are 13 distinct labels, voyaging from 12 to 30 regarding $k$ is helpful due to the fact that there can be multiple unsupervised topics refer to one label. I trained all models with a held-out sample of 10\% of the data.

Based on the semantic coherence scoring\footnote{Details please see their original quantification in the original paper on page 265. In short, supposed we define a trained topic $V^{(k)}$ with the $M \in [2,\inf)$ top words $v_1^{(k)} \cdots v_M^{(k)}$ that are selected by some criteria. Then, given a topical construct and for each word $v_l^{(t)}$ in it, we compute the log-ratio between the frequency of the co-occurrence $v_m^{(k)}$ \& $v_j^{(l)}$ ($m \neq l$), and the frequency of $v_l^{(k)}$. Formally (adapted from the original paper):

\begin{align*}
    C(k, V^{(k)}) = \sum^{M}_{m=2}\sum^{m-1}_{l=1} \log \frac{D\left(v_m^{k}, v_l^{k} \right) + 1}{D\left( v_l^{k}\right)}
\end{align*}

Note that this frequency function, $D(v)$, will be applied if and only if the to-be-computed tokens are present. Then, these log-ratios will be summed up for a topic model. The larger the metric is, so the authors argue, the more coherent the topic words are related to each other. However, I note that this measurement may not be the most plausible one. Consider the scenario where two words are distinct concepts but appear jointly frequently - The score of the cluster containing those two words would take such observations as positive cases regardless of the theorizing and induces a biased estimate.} \cite{mimno2011optimizing} and held-out likelihoods, I chose 13 models to train in the second round. Among the remaining models, I selected 2 models based on the the metrics related to the word-compositions regarding topical features\footnote{Here, I use the FREX-score proposed by \cite{bischof2012summarizing}:

\begin{align*}
FREX(w|k) = \left(\frac{\delta}{F_k(w)} + \frac{1-\delta}{\beta_{w,k}}\right)^{-1}
\end{align*}
where $w$ is a word and $k$ represents a topic. $F_k(w)$ represents the cumulative probability of a word in a given topic, i.e. the frequency component; $\beta_{w,k}$ denotes a word's normalized loading on a given topic, i.e. the exclusivity component. $\delta$ can be arbitrarily set within $[0,1]$ to balance the trade-off between these two components. I used $\delta = 0.5$ in this study, i.e., a balance between the frequency and exclusivity. Note that this is not the only way to determine the composition and the order regarding the top words.}, including semantic score, Jaccard similarity coefficients, as well as qualitative interpretations. In the end, the models with $k = 25, \alpha = 2$ and $k = 25, \alpha = 0.8$ appear most plausible.

\begin{figure}[H]
    \centering
    \caption{Examples from $k = 25, \alpha = 0.8$}
    \label{fig:step_1_avg_prob}
    \begin{subfigure}[t]{.49\textwidth}
    \centering
    \includegraphics[width=\linewidth]{step_1_avg_prob/medicine.png}
    \caption{Example: Single-Membership}\label{fig:step_1_avg_prob_a}
    \end{subfigure}
%
    \begin{subfigure}[t]{.49\textwidth}
    \centering
    \includegraphics[width=\linewidth]{step_1_avg_prob/urban.png}
    \caption{Example: Mixture-Membership}\label{fig:step_1_avg_prob_b}
    \end{subfigure}
    
    \medskip
    
    \begin{subfigure}[t]{.49\textwidth}
    \centering
    \includegraphics[width=\linewidth]{step_1_avg_prob/bureaucracy.png}
    \caption{Example: Unidentified Relation}\label{fig:step_1_avg_prob_c}
    \end{subfigure}
%
    \begin{subfigure}[t]{.49\textwidth}
    \centering
    \includegraphics[width=\linewidth]{step_1_avg_prob/culture.png}
    \caption{Example: Misidentification}\label{fig:step_1_avg_prob_d}
    \end{subfigure}
    
\end{figure}

To analyze the association between the topical features and the human-generated labels, the average probability of all topical features given a label was computed. I selected some typical labels and show the content the a topic when the average exceeds $0.1$. The content consists of the top five words and is translated automatically through the Google Cloud Translation API.\footnote{Website and instructions: \url{https://cloud.google.com/translate/}}

Figure \ref{fig:step_1_avg_prob} depicts four examples from the model with $k = 25, \alpha = 0.8$. There exist some labels where plausible relations between the topical features and the human-generated labels are well identified, be it in a single- or mixed-membership mode. For instance, ``see a doctor", ``surgery", ``Xinnong" (New Agriculture), ``serious illness", and ``drug" constitutes a topic that is strongly related to the label of Medicine (Figure \ref{fig:step_1_avg_prob_a}), which is in general credible. However, there are also labels like Bureaucracy which is identified ambiguously, or Culture/Entertainment which is even misidentified (Documents with high probability in tourism and urban-development related are labeled with it, as the plot in Figure \ref{fig:step_1_avg_prob_d} suggests.). 

The above investigation already reveals several interesting links. What can be learned is not only the plausibility of the topic-label relations but also the potential reasons behind their quality. For instance, regarding the label Urban Planning, the most probable topical features only partially reflect its content. Important facets from expert views like infrastructure are not identified here. In the second stage, a more analytic aspect regarding such relationships will be unfolded, which might help with the understanding.

\section{Inference from Topical Features}

At the second stage, I used a single-layer multi-class SVM to classify documents. The codes are written from scratch and largely based on \texttt{numpy} and \texttt{pandas}. For mitigating the overfitting, I used the batch stochastic gradient descent to train the model. The batch sample (size = 688), as well as the validation set (size = 562) are rather balanced than representative. For instance, label 4 Urban Planning makes up 31 percent of the training data. In the batch sample, it only makes up roughly 10 percent. The term ``stochastic" here indicates that the elements of the batch sample and the ones in the validation set are updated in each of the iteration, which also safeguards the avoidance of overfitting. For detailed information, please refer to the gitlab repository (step\_2/SVM\_reweight\_info.csv).

%\footnote{In practice, I change the size for several times, but it does not affect the overal accuracy at all in the end.}

In constructing the feature set, I consider three scenarios. Besides the simplest one, i.e., including the topical features only, I add a covariate \textit{year} in the second model. I assume that in different years, the content of the labels might be perceived differently. The third model is just an extension to include every interactive terms between the topical features and the years. Put it formally:

\begin{align}
    \hat{Y} &= f_{svm}\left(a + X_{topic}\beta\right) \\
    \hat{Y} &= f_{svm}\left(a + X_{topic}\beta + YEAR\theta\right) \\
    \hat{Y} &= f_{svm}\left(a + X_{topic}\beta + YEAR\theta + \left(\sum^{||topic||}_{i = 1}\sum^{||year||}_{j = 1} X_{i}YEAR_{j}\right)\eta\right)
\end{align}

where $f_{svm}(X)$ is the multi-class SVM function, $X_{topic}$ refers to the topical features and $YEAR$ the year dummy variable. Regarding the inferences, we are mostly interested in the point estimate of $\beta, \theta, \eta$.

In terms of margins (set to 1), regularization (set to $L2$), and learning rates (set to $10^{-5}$), I tried different combinations, and the results remain roughly the same across different settings. Figure \ref{fig:accuracy} reports the loess of validation accuracy in 4000 iterations by the model of $k = 25, \alpha = 2$ (Model 1) and $k = 25, \alpha = 0.8$ (Model 2). It is easy to see that the single-membership feature set ($\alpha = 0.8$) performs better than the mix-membership one ($\alpha = 2$). Model 2 almost reaches 60\% of validation accuracy in the simplest feature set, where Model 1's performance is between 55\% and 57\% overall. The improvement of the accuracy with the most complicated feature set indicates that year is an important covariate; However, as the amount of independent variables is rising, the improvement can be also induced by overfitting.

In the following in-depth analysis, I concentrate on the results from Model 2 ($k = 25, \alpha = 0.8$). First, the conventional accuracy metrics will be presented. Then, the weights of topical features will be critically discussed.

\begin{figure}[t]
    \centering
    \includegraphics[width=\textwidth]{accuracy.pdf}
    \caption{Validation Accuracy}
    \label{fig:accuracy}
\end{figure}

\subsection{Accuracy Metrics}
Table \ref{tab:acc_topic} depicts the precision, recall, and F1 score of each class in the training set (Calculation based on the multi-class generalization in \citeA{sokolova2009systematic}), based on the simplest feature set that only consists of topical features. There exists no clear relation between the size of the class and their F1 score, which indicates that the model is not prone to the salient cases. In general, besides the two classes ``Culture/Entertainment" and ``Finance" where the size is small, all other classes are predicted. Labels such as Bureaucracy, Public Security, and Business receive low F1, which indicates that the data-generating process of such labels might be more complicated and subjects to a significant level of variance concerning expressions.

\begin{table}[ht]
\caption{Accuracy Metrics (Only Topical Features)}
\label{tab:acc_topic}
\centering
\begin{tabular}{lrrrrr}
  \hline \hline
 Label & Precision (in \%) & Recall (in \%) & F1 (in \%) & Size & Relative Size \\ 
  \hline
Agriculture & 44.85 & 52.53 & 48.39 & 771 & 0.16 \\ 
Transportation & 46.56 & 61.70 & 53.07 & 329 & 0.07 \\ 
Business & 32.28 & 39.87 & 35.67 & 153 & 0.03 \\ 
Medicine & 70.92 & 62.02 & 66.17 & 287 & 0.06 \\ 
Urban Planning & 87.59 & 67.22 & 76.07 & 1565 & 0.32 \\ 
Employment & 37.71 & 55.93 & 45.05 & 354 & 0.07 \\ 
Bureaucracy & 15.17 & 16.22 & 15.67 & 333 & 0.07 \\ 
Education & 68.88 & 71.54 & 70.19 & 492 & 0.10 \\ 
Culture/Entertainment &  & 0.00 &  &  24 & 0.00 \\ 
Tourism & 31.82 & 56.00 & 40.58 &  50 & 0.01 \\ 
Public Security & 25.66 & 17.68 & 20.94 & 164 & 0.03 \\ 
Environment & 68.29 & 61.28 & 64.59 & 390 & 0.08 \\ 
Finance &  & 0.00 &  &  11 & 0.00 \\ 
   \hline \hline
\end{tabular}
\end{table}

In Table \ref{fig:accuracy}, we observe an overall improvement in classification accuracy, when the covariate ``year" and the interactive terms between it and the topical features are included. Table \ref{tab:acc_all} examines the class-level contributions. I find strong support for including the year as an additional and interactive covariate: There are six classes whose F1 scores are improved by more than 3 percent. Importantly, the class ``Culture/Entertainment" is identified in this setting with an F1 of 31.26\%. Besides the label ``Business", all other labels' accuracy increase or stays at the previous level. 

\begin{table}[H]
\caption{Accuracy Metrics (Including the Year-Covariate and Interaction Terms)}
\label{tab:acc_all}
\centering
\resizebox{\textwidth}{!}{\begin{tabular}{lrrrrrr}
  \hline \hline
 Label & Precision (in \%) & Recall (in \%) & F1 (in \%) & $\Delta_{F1}$& Size & Relative Size \\ 
  \hline
Agriculture & 45.11 & 54.99 & 49.56 & 1.17 & 771 & 0.16 \\ 
\textbf{Transportation} & 49.65 & 65.05 & 56.32 & \textbf{3.25} & 329 & 0.07 \\ 
Business & 27.00 & 41.83 & 32.82 & -2.85& 153 & 0.03 \\ 
Medicine & 71.49 & 62.02 & 66.42 &0.25& 287 & 0.06 \\ 
Urban Planning & 87.35 & 68.37 & 76.70 &0.63& 1565 & 0.32 \\ 
\textbf{Employment} & 45.88 & 58.19 & 51.31 &\textbf{6.26}& 354 & 0.07 \\ 
\textbf{Bureaucracy} & 23.56 & 24.62 & 24.08 &\textbf{8.41}& 333 & 0.07 \\ 
Education & 69.88 & 72.15 & 71.00 &0.81& 492 & 0.10 \\ 
\textbf{Culture/Entertainment} & 71.43 & 20.83 & 32.26 &\textbf{32.26}& 24 & 0.00 \\ 
\textbf{Tourism} & 74.19 & 46.00 & 56.79 &\textbf{16.61}& 50 & 0.01 \\ 
\textbf{Public Security} & 27.56 & 21.34 & 24.05 &\textbf{3.11}& 164 & 0.03 \\ 
Environment & 66.85 & 63.59 & 65.18 &0.59& 390 & 0.08 \\ 
Finance &  & 0.00 &   &0.00&   11 & 0.00 \\
   \hline \hline
\end{tabular}}
\end{table}

\subsection{SVM-Weights}
The accuracy metrics presented above do not reveal whether the model can accurately predict the \textit{true} labels of political texts. Recalling that citizens generate these labels, there can be a variety of reasons for an individual to misreport the labels. These quantifications merely report a general fit between two data-generating processes. For evaluating the models in-depth, I critically scrutinize the SVM-weights in this subsection.

First, we look at the weights from the simplest model with only topical features. Due to the limited space, I picked the following six labels that form a representative spectrum: Two cases with a high F1 score (Medicine ($66.17$), Urban Planning ($76.07$)), two cases with a medium-low one (Employment ($45.05$), Business ($35.07$)), one case with low a F1 (Bureaucracy ($16.22$)), and the unidentified case of Culture/Entertainment. Figure \ref{fig:step_2_weight_topic} visualizes the weights and prints the five top words of the features when the loading exceeds $0.01$. 

In Figure \ref{fig:step_2_weight_topic_a} and \ref{fig:step_2_weight_topic_b}, these two well classified labels also show a clear inference between topical features and human-generated labels. Medicine is a narrow issue that is easy to be identified through idiosyncratic tokens. Urban Planning, in turn, offers a great spectrum for individual specifications, be it related property ownership, loans for housing purchase, or real-estate developments. However, important facets such as infrastructure, other public goods and services are not identified with this label.

Labels which a predicted with the accuracy of tossing a fair coin are likely the ones that often share facets with other labels. The empirical practices challenge the seemingly mutual exclusivity. Figure \ref{fig:step_2_weight_topic_c} and \ref{fig:step_2_weight_topic_d} show that the same topical feature have similar loading both in Employment and Business issues. This 25th feature reads in its 20 top words:

\begin{itemize}
\item Chinese: \begin{CJK*}{UTF8}{gbsn}
结清, 赵庄村, 班组, 王伍, 清欠办, 佰工, 农民工, 老板, 干活, 打工, 过年, 讨要, 劳动局, 工地, 五洲, 干部职工, 邢云, 工人, 监察 
\end{CJK*}
\item English: Settling [money transfer], Zhaozhuang village, Team [in an enterprise], Wang Wu (probably a name), Office of settling unpaid salary (a government agency), Baigong (a private enterprise), migrant workers, boss, work, work, celebrate the new year, require, Bureau of labour affairs, construction side, Wuzhou (a company), cadres and workers, Xingyun (probably a name), workers, inspection
\end{itemize}

Here, it becomes clear that people may classify their texts regarding salaries or employment status to Business issues due to the links with enterprises. A speculated theory can be that some citizens would like to pressure on the enterprises through such a classification, because the matters related to enterprises may be handled more seriously than mere labor issues. For instance,According to the data from Qichacha, a data collection service in China, Baigong, one of the mentioned company in the top words, has been sued for more than a thousand times.\footnote{Date retrieved from \url{https://www.qichacha.com/cbase_a0de6f730bc7a366b4a946fe362ccdd1} at the 25th of January, 2018.} 

\begin{figure}[H]
    \centering
    \caption{SVM-Weights from Model based on Topical Features}
    \label{fig:step_2_weight_topic}
    \begin{subfigure}[t]{.45\textwidth}
    \centering
    \includegraphics[width=\linewidth]{step_2_weight_topic/medicine.png}
    \caption{Example: Single Inference $(F1 = 66.17)$}\label{fig:step_2_weight_topic_a}
    \end{subfigure}
%
    \begin{subfigure}[t]{.45\textwidth}
    \centering
    \includegraphics[width=\linewidth]{step_2_weight_topic/urban_planning.png}
    \caption{Example: Multiple Inferences $(F1 = 76.07)$}\label{fig:step_2_weight_topic_b}
    \end{subfigure}
    
    \medskip
    
    \begin{subfigure}[t]{.45\textwidth}
    \centering
    \includegraphics[width=\linewidth]{step_2_weight_topic/employment.png}
    \caption{Example: Partial Identification $(F1 = 45.05)$}\label{fig:step_2_weight_topic_c}
    \end{subfigure}
%
    \begin{subfigure}[t]{.45\textwidth}
    \centering
    \includegraphics[width=\linewidth]{step_2_weight_topic/business.png}
    \caption{Example: Partial Identification $(F1 = 35.07)$}\label{fig:step_2_weight_topic_d}
    \end{subfigure}
    
    \begin{subfigure}[t]{.45\textwidth}
    \centering
    \includegraphics[width=\linewidth]{step_2_weight_topic/bureaucracy.png}
    \caption{Example: Weak Inference $(F1 = 16.22)$}\label{fig:step_2_weight_topic_e}
    \end{subfigure}
%
    \begin{subfigure}[t]{.425\textwidth}
    \centering
    \includegraphics[width=\linewidth]{step_2_weight_topic/culture_entertainment.png}
    \caption{Example: "No" Inference $(F1 = 0)$}\label{fig:step_2_weight_topic_f}
    \end{subfigure}
    
\end{figure}

Now we turn to the labels that are poorly classified. Bureaucracy (Figure \ref{fig:step_2_weight_topic_d}) seems to able to inferred from a variety of topical features. It does not necessarily suggest that labeling some concerns as Bureaucracy is random, but the practice is definitely subject to individual heterogeneity. In Figure \ref{fig:step_2_weight_topic_e}, there exist no weights that exceed $0.01$ that are qualified to infer the label of Culture/Entertainment.


However, as previously shown, taking the covariate year and its related interactive terms enhances the accuracy greatly. I show three examples here. The weights are calculated through the addition of the weights of the topical features, and their weights of the interactive terms given a specific year. First, let us take a look at the issue of Culture/Entertainment here (Figure \ref{fig:step_2_weight_year_culture}). It appears that in 2015 and 2016, the inference from the topical feature that consists of Memorial, Latin Dance etc. is particularly strong. Although this topical feature also strongly infer the label of Tourism in the simplest and the complicated model, the strongest inference there is in 2014, 2015 and 2018. It suggests that year might have a substantial explanatory power regarding the link between the machine and the human data-generating processes.  

\begin{figure}[!htbp]
    \centering
    \caption{SVM-Weights regarding Culture/Entertainment according from 2014 to 2018 (Topical Features with the Covariate and the Interactive Terms)}
    \label{fig:step_2_weight_year_culture}
    \includegraphics[width=0.95\linewidth]{step_2_weight_year/Culture_Entertainment.pdf}
\end{figure}

Further, the inferential power of the model must not be associated with the conventional accuracy metrics. Figure \ref{fig:step_2_weight_year_finance} gives some thoughts: The topical feature on the top of the subplot in 2018 was specified above, which is strongly related to employment, enterprises, and in particular unpaid salaries. All of the texts labeled with finance stems from the year 2018, which explains why its weight in 2018 is stronger than any other time. Among the 11 cases under this label, two of them (roughly 20\%!) shall be related to employment issues for they are related to unpaid salaries.
\begin{figure}[H]
    \centering
    \caption{SVM-Weights regarding Finance according from 2014 to 2018 (Topical Features with the Covariate and the Interactive Terms)}
    \label{fig:step_2_weight_year_finance}
    \includegraphics[width=0.95\linewidth]{step_2_weight_year/Finance.pdf}
\end{figure}

At last, I highlight another dimension of regarding the data-generating process. Figure \ref{fig:step_2_weight_year_environment} depicts the weights of the label Environment. From 2014 to 2018, two topical features significantly contribute to the classification of related texts to this issue: Air pollution (odor, bad smell etc.) and infrastructure-related (sediment, lane etc.) topics, where the latter one has a comparably smaller size. However, starting from 2017, a seemingly unrelated topic that is constituted by the words regarding the heating and the natural gas also stands out in classifying environmental issues. This evidence corroborates the facts that the Chinese government has been banning heating from burning coal to fight air pollution, but this policy unintentionally cut down the heating resources in the rural area in North China.\footnote{For a sample media report, please see \url{https://www.nytimes.com/2018/02/10/world/asia/china-coal-smog-pollution.html}, retrieved at the 25th of January, 2019.}

\begin{figure}[H]
    \centering
    \caption{SVM-Weights regarding Environment according from 2014 to 2018 (Topical Features with the Covariate and the Interactive Terms)}
    \label{fig:step_2_weight_year_environment}
    \includegraphics[width=0.95\linewidth]{step_2_weight_year/Environment.pdf}
\end{figure}

\section{Prediction Analysis}

Until here, I have deliberately analyzed the prediction model. However, since the test set is unlabeled with substantive issues, it is hard to use the same way to judge the quality in this section. Here, I briefly report the distribution of the predicted labels and their relation with topical features. %At last, I train some topic models only on the test set, and then run a multivariate regression with the newly generated topical features.

\subsection{Distribution of Predicted Labels}

The predicted distribution in the "Other"-category does not mimic the strongly imbalanced distribution in the training data. In the most sophisticated feature set, Culture/Entertainment was identified for one case. However, through the investigation in the original data, almost all the cases identified as ``Culture/Entertainment" or ``Tourism" are actually issues that are related to electricity supply or fire potential - However, in the default categorization, such issues do not exist.\footnote{The subjects of these falsely categorized cases are: (1) ``About the major fire in the Taoshan market in Guantao County on April 11"; (2) "Frequent power outages, hardships in people's livelihood, seriously affecting people's lives"; (3) "Difficult to diagnose electric meter"; (4)"Tang County, Shijiazhuang, Hebei Province"; (5)"Safety hazards in new buildings such as Jinguiyuan Square in Xuefu Road, Shijiazhuang"; (6) "Broadband in Jintao Lidu Community, Guantao County"}

\begin{table}[ht]
\centering
\caption{Distribution of Predicted Labels ($k = 25, \alpha = 0.8$)}
\label{tab:pred_dist}
\begin{tabular}{lrrr}
  \hline\hline
Label & All & With Covariates & Only Topical Features \\ 
  \hline
Agriculture & 215 & 202 & 197 \\ 
 Transportation &  52 &  52 &  50 \\ 
 Business &  79 &  70 &  65 \\ 
 Medicine &  40 &  40 &  42 \\ 
 Urban Planning & 226 & 215 & 215 \\ 
 Employment & 205 & 201 & 207 \\ 
 Bureaucracy &  97 & 149 & 152 \\ 
 Education &  58 &  59 &  59 \\ 
 Culture/Entertainment &   1 & 0 & 0 \\ 
 Tourism &   1 &   5 &   6 \\ 
Public Security &  67 &  52 &  52 \\ 
 Environment &  36 &  32 &  32 \\ 
 Finance & 0 & 0 & 0 \\ 
   \hline\hline
\end{tabular}
\end{table}

Another systematic look at the prediction effort is to determine whether most of the models predict a text with the same label. Among all the models related to the topical features trained by $k = 25, \alpha = 0.8$, there are 831 cases ($77.2\%$) where three models mutually agree upon, 242 ($22.5\%$) cases where only two models predict the same label and 4 cases ($0.3\%$) where three models return different results.

\begin{figure}[H]
    \centering
    \caption{Average Probability of Topical Features Given Predicted Labels in Test Data}
    \includegraphics[page = 1, width = \textwidth]{k25_a0_8_all.pdf}
    \label{fig:pred_avg}
\end{figure}

\begin{figure}[H]
    \centering
    \caption{Average Probability of Topical Features Given Predicted Labels (Continued)}
    \includegraphics[page = 2, width = \textwidth]{k25_a0_8_all.pdf}
    \label{fig:pred_avg_cont}
\end{figure}

\subsection{Topical Features and Machine-Predicted Labels}

Regarding the average probability of topical features given a predicted label, Figure \ref{fig:pred_avg} and \ref{fig:pred_avg_cont} provides the results from the topical features against the predictions from the sophisticated model with interactive terms. Eyeballing the results, the distribution seems to be accurate. Recalling the previous discussion on the intersection between Employment and Business issues, here we observe already some distinctions: topics such as social welfare (Maternity leave etc.) and jobs in the public sector (township cadres etc.) are most salient under the Employment label than in the Business issue.

%\subsection{Match between Isolated Test Set Training and Predicted Labels}

%As mentioned in the introductory section, this showcase has a strong assumption that the ``Other" category can be treated as failing to classify a document to a substantive topic. However, some results discussed in the previous section challenge this assumption: Given that the substantive topics provided on that online platform are not exhausted, it could be fact that some documents are de facto unclassifiable.

\section{Discussion}
In this task, I performed the semi-supervised learning with political textual data generated in a particular setting: The texts are generated by the citizens living in an authoritarian regime. It is presumed that the texts are categorized with substantive issues, as well as the ones with ``Other" categorization, stem from the same data-generating process. However, the data-generating process regarding label selection might differ. 

The goal of this exercise is not only about the accuracy, but also the inference we may draw from the machine-learned textual data-generating process to the human-labeled practices. Throughout the reports, I also combine and review different evaluation strategies to reveal this relationship. For avoiding fishing problems, I trained multiple models with different parameter-pairs at the first stage, and also a variety of feature sets in the supervised learning. Although the scope of the search and the comparison is still relatively small, the analytical logic presented in this paper can still be applied when the model choices are scaled-up.

Besides some clear supportive evidence that seems to be plausible already at first glance, several controversies also emerge: First, issues that naturally share the same or similar concerns are hard to be identified. Second, with the evolution of time or general contexts, the extent of a topic might change. Third, non-salient topics could be absorbed in more salient ones. Last but not least, the initial constraints on the human-generation of labels - The degree of mutual exclusivity \textit{and} the exhaustiveness of the issue spectrum - might also influence at least the human-classification in the first place. 

The current model is able to identify the above-mentioned problems, yet the solutions deserve a separate discussion: First, how can the evaluation, or the diagnosis be improved? Second, what are the potential solutions to counteract these problems? I offer some thoughts below.

First of all, the model selection in the first step - which is a more general caveat of topic models - can subject to fishing problems. In this exercise, I did not carry out a grid-search with many parameter-pairs. However, as readers can scrutinize the topic-words across the models being trained, there exist many linguistic similarities. Nevertheless, it is important to note that either the held-out likelihoods or the semantic coherence proposed by \citeA{mimno2011optimizing}, cannot fully capture the "goodness" of the model. Future research might involve (1) n-gram extensions in the corpora-producing stage, and (2) exploiting the types of words using speech-tagging to produce more fine-grained topical loadings.

Second, as all empirical research regarding the inference must acknowledge, the theoretical foundation needs to be justified through critical argumentations. In this exercise, I only include the year as the only covariate. However, this covariate might not be relevant, as suggested in the results. Here, we must seek political behavior/psychological foundations to find concepts that are of relevance, and further the appropriate measurements for it. There is but also a technical side: Provided that the covariates are found - In what relation they are linking with the topical features? I.e., are they only affecting the generating-procedure of the labels ($Y$), or also the topical features ($X_{topic}$)? The latter part deserves a separate discussion in the future.

Third, how shall the quality of the topical features to be interpreted? I chose the FREX-algorithm to label the topical features. However, since they are produced by *clustering*, the topical features are not mutually exclusive of one and other even though they are treated as if they were discretely distributed in a Dirichlet allocation. The overlapping, in my view, shall be critically measured and shown with a mixture model (hierarchical model). Further, there exist some topical clusters that cannot be interpreted - Shall we discard them or not? Can it be viewed as an "error" cluster that might summarize "omitted topical clusters" altogether? Last but not least, as $k$ is shrinking, I observe some "absorption" of non-salient topical features into salient ones. This can be tested when some high-quality labels exist; However, in a pure unsupervised learning scenario, it is sometimes hardly detectable.

The common practice to the third problem is to label (at least a partial set) of the test data. Nevertheless, it might invite an additional data-generating procedure that is not the concern of the research question. In the empirical scenario of this paper, even if experts in political science labeled the test set, their motivation to label them would be distinguish from the reasoning of the citizens. Recalling the puzzle between the Employment and Business issues in the report, the expert-coding might lead researchers to detect the other kind of label-generating practices.

In the short-term, some techniques can be employed. First, training the test data in an isolated way, and then inspecting the inference from the newly generated topical features to the predicted labels. The inference can be conducted for the whole test data, as well as according to the quality of prediction (i.e., how much mutual agreement a text receives from different models regarding the prediction).

Second, instead of generating new labels, we can invite people to evaluate the coherence between text in the test set and its predicted label, given that the respondents are aware of how some training data are labeled. In essence, this procedure replicates Bayesian learning for the respondents. Rather than labeling the texts, to assign their perceived credibility of the prediction is a more direct way to assess the plausibility of the detected data-generating process.

Last but not least, researchers might be interested in questions that particularly lie at the measurement or prediction failures. The given label-set might not be mutually distinguishable or exhaustive. Why and when citizens choose one over the other, or just do not assign a substantive topic when such a problem arises, requires further exploration.