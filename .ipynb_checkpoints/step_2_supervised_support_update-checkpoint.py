import numpy as np
import pandas as pd
from past.builtins import xrange

def svm_loss_vectorized(W, X, y, reg):
    """
    Structured SVM loss function, vectorized implementation.

    Inputs and outputs are the same as svm_loss_naive.
    """
    loss = 0.0
    dW = np.zeros(W.shape) # initialize the gradient as zero

    num_classes = W.shape[1]
    num_train = X.shape[0]
    
    scores = np.dot(X,W)
    
    correct_class_scores = scores[range(num_train),y]
    
    margins = scores - correct_class_scores[:, None] + 1
    margins[range(num_train),y] = 0
    loss = np.sum(np.maximum(0,margins))/num_train
    loss += reg * np.sum(W[:W.shape[0]-1,:] * W[:W.shape[0]-1,:]) 

    
    # partial gradients for each training example:
    slopes = np.zeros((margins.shape)) # default case for correct classifications
    slopes[margins > 0] = 1            # where j = k
    slopes[range(num_train),y] -= np.sum(slopes, axis = 1) # sum over classes
    
    dW = np.dot(X.T,slopes)
    dW /= num_train
    
    # -1 indexing to exclude bias in normalization
    dW[:W.shape[0]-1,:] += reg * 2 * W[:W.shape[0]-1,:]

    return loss, dW

def softmax_loss_vectorized(W, X, y, reg):
    """
    Softmax loss function, vectorized version.
    Inputs have dimension D, there are C classes,
    and we operate on minibatches of N examples.

    Inputs:
    - W: A numpy array of shape (D, C) containing weights.
    - X: A numpy array of shape (N, D) containing a minibatch of data.
    - y: A numpy array of shape (N,) containing training labels; y[i] = c means
         that X[i] has label c, where 0 <= c < C.
    - reg: (float) regularization strength

    Returns a tuple of:
    - loss as single float
    - gradient with respect to weights W; an array of same shape as W
  
  

    Inputs and outputs are the same as softmax_loss_naive.
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)
    
    num_samples = X.shape[0]

    scores = np.dot(X,W)
    scores -= np.max(scores,axis=1,keepdims=True)
    p = np.exp(scores)/np.sum(np.exp(scores),axis=1,keepdims=True)
    
    loss = -np.sum(np.log(p[range(num_samples),y]))
  
    p[range(num_samples),y] -= 1
    
    dW = np.dot(X.T,p)
    
    
    loss /= num_samples
    dW /= num_samples
    
    loss += reg * np.sum(W * W)
    dW += reg * 2 * W

    return loss, dW


def train_SGD(X, y, val_p, batch_p, learning_rate=1e-3, reg=1e-5, num_iters=100,
             verbose=False):
    """
    Train this linear classifier using stochastic gradient descent.

    Inputs:
    - X: A numpy array of shape (N, D) containing training data; there are N
      training samples each of dimension D.
    - y: A numpy array of shape (N,) containing training labels; y[i] = c
      means that X[i] has label 0 <= c < C for C classes.
    - learning_rate: (float) learning rate for optimization.
    - reg: (float) regularization strength.
    - num_iters: (integer) number of steps to take when optimizing
    - batch_size: (integer) number of training examples to use at each step.
    - verbose: (boolean) If true, print progress during optimization.

    Outputs:
    W: A numpy array of shape (D, C) containing weights
    loss_history: A list containing the value of the loss function at each training iteration.
    """
    num_train, dim = X.shape
    num_classes = np.max(y) + 1 # assume y takes values 0...K-1 where K is number of classes
   
    # Generate a random softmax weight matrix
    W = 0.001 * np.random.randn(dim, num_classes)
    
    # Stratified sampling
    unique_count = np.bincount(y)
    
    # Run stochastic gradient descent to optimize W
    loss_history = []
    train_acc_history = []
    val_acc_history = []
    val_acc_old = 0.0
    
    
    # Begin of iterative training
    
    for it in xrange(num_iters):
        # Define validation set
        val = []
        for i in list(set(y)):
            num_temp = unique_count[i]
            val_temp_pos = np.random.choice(num_temp, int(num_temp*val_p[i]))
            real_pos = np.where(y == i)
            val_temp = real_pos[0][val_temp_pos]
            val.append(val_temp)
            
            
        val = [item for sublist in val for item in sublist]
        
        # Define training set
        train = np.setdiff1d(list(range(num_train)), val)
        
        X_val = X[val,:]
        X_train = X[train,:]
        y_val = y[val]
        y_train = y[train]
        
        # Define batch set
        batch = []
        unique_count_train = np.bincount(y_train)
        for i in list(set(y)):
            num_train_temp = unique_count_train[i]
            batch_temp_pos = np.random.choice(num_train_temp, int(num_train_temp*batch_p[i]))
            real_pos = np.where(y_train == i)
            batch_temp = real_pos[0][batch_temp_pos]
            batch.append(batch_temp)
            
        
        X_batch = None
        y_batch = None
        
        batch = [item for sublist in batch for item in sublist]
        X_batch = X_train[batch,:]
        y_batch = y_train[batch]
  

        # evaluate loss and gradient
        loss, grad = svm_loss_vectorized(W, X_batch, y_batch, reg)
        loss_history.append(loss)
 
        W -= learning_rate*grad
        
        y_train_pred = np.argmax(np.dot(X_train,W),axis=1)
        y_val_pred = np.argmax(np.dot(X_val,W),axis=1)
        
        # accuracy history
        train_acc = np.mean(y_train == y_train_pred)
        train_acc_history.append(train_acc)
        
        val_acc = np.mean(y_val == y_val_pred)
        val_acc_history.append(val_acc)
        
        # updating the best weight matrix
        if val_acc > val_acc_old:
            best_W = W
        else:
            best_W = best_W
        
        val_acc_old = val_acc
        
        if verbose and it % 100 == 0:
            print('iteration %d / %d: loss %f train_acc %f val_acc %f' % (it, num_iters, loss, train_acc, val_acc))
    
    return W, best_W, loss_history, train_acc_history, val_acc_history


def predict(W, X):
    y_pred = np.zeros(X.shape[0])
    y_pred = np.argmax(np.dot(X,W),axis=1)
    return y_pred

def acc_class(y_pred, y_ori):
    acc_class = []
    class_length = []
    for i in xrange(len(set(y_pred))):
        temp = np.sum(y_pred[y_ori == i] == y_ori[y_ori == i])
        temp /= len(y_ori[y_ori == i])
        
        acc_class.append(temp)
        class_length.append(len(y_ori[y_ori == i]))
    
    #class_prop = np.array(class_length)/len(y_ori)
    np.set_printoptions(precision=4, suppress = True)
    
    a = pd.DataFrame({'class_name': np.arange(len(set(y_pred))), 'accuracy': acc_class, 'class_size': class_length})
    
    #a = a.round(2)
    return(a)