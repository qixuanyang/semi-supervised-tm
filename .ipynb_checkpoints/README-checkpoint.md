# Semi-supervised Topic Model

Qixuan Yang (Last update 26.01.2019)

The respository demonstrates the approach provided by the following article

***Yang, Qixuan and Lukas Ringlage (2018). Semi-Supervised Learning of Topic Classification: German Parliamentary Inquiries as an Example. Unpublished Manuscript. University of Konstanz.*** 

This seminar work was accomplished in the Machine Learning course taught by Prof. Lyudmila Grigoryeva (Summer term 2018), and supported by the Working Group of Comparative Politics at the University of Konstanz. Due to the issues related to intellectual property, I demonstrate the approach here by using an alternative data source in Chinese. 

For the formal technical report, please click [here](report_latex/technical_report.pdf).

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Brief notes](#brief-notes)
- [Architecture](#architecture)
- [Data: Chinese Citizens Complaints to Provincial Government - An Example from Hebei Province](#data-chinese-citizens-complaints-to-provincial-government---an-example-from-hebei-province)
- [Results](#results)
- [Techinical Remarks](#techinical-remarks)
  - [Preprocessing of data](#preprocessing-of-data)
  - [First stage: Unsupervised learning - Generating topical attributes](#first-stage-unsupervised-learning---generating-topical-attributes)
  - [Second stage: Supervised learning - Predicting uncategorized texts](#second-stage-supervised-learning---predicting-uncategorized-texts)
- [Limitations and Future Research](#limitations-and-future-research)
- [Related Literature](#related-literature)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Brief notes

In the original paper, we used the annotated collective parliamentary inquiries (*Kleine/Große Anfrage*) from the 15th to the 18th legislative periods (1998 - 2017) in the Federal Republic of Germany as the training set. The testing set consists of personal/oral parliamentary inquiries in the same period. The topical annotation was provided by the Working Group of Comparative Politics at the University of Konstanz, directed by Prof. Christian Breunig and a former member, Prof. Tinette Schnatterer. The topics and their definitions are determined by Comparative Agendas Project (CAP: https://www.comparativeagendas.net), an international political text database initiative.

__Our validation accuracy scores at best with 69.76% with macro-labels (7-label multivariate distribution), and 56.93% with micro-labels (21-topic multivariate distribution) with one-layer SVM (Support Vector Machine) classifier.__ Especially in terms of a 21-label scenario, there exists a significant difference between salient and non-salient topics. We used batch balanced sampling technique to prevent the overfitting and proneness towards larger classes, which improves the accuracy of non-salient topics while preserving an overall high accuracy. We even succeeded in extracting plausible partisan inferences, which highly correlates with existing research on partisan issue-ownership in contemporary Germany.

The paper could be viewed on the individual basis, with the consent of the co-author (Lukas), the data provider (Christian and Tinette), as well as the supervisor (Lyudmila) who assists us complete this project in her Machine Learning seminar in 2018. We thank the comments of all the participants in the Machine Learning course, specifically the helpful insights from Lyudmila. We are also grateful for the data provided by Christian and Tinette, especially that they share this information with us.

## Architecture

In designing the architecture, we perform two tasks. First, our ultimate interest lies at the inference from unsupervised topical construct to human-labeled practices. Second, we want to understand, rather than presuppose, the data generating process of (political) textual data.

Given the context of partial labeling, we propose the following simple structure

![alt text](readme_pics/Architecture_screen_shot.png)

In the first step, we use a topic model (e.g., latent Dirichlet allocation) to derive the posterior distribution of the document topic-domain according to the topic-related texts, as well as the list of words clustered in each domain. These lists of word representations are thus the semantic predictors ($`X_{topic}`$) of our feature set ($`Y`$). Further, we add some other document-based covariates, if applicable and plausible, into the feature set. In the second stage, we use multi-class supervised learning to train the model and predict the labels of test data.

Two-stage semi-supervised learning, we argue, is more suitable for the context where the data stemming from the same generating procedure is only partially labeled, and where the word features might not be identically or similarly distributed among test and training set. It suggests that this model might be more computational expensive since there are two rounds of training, but has weaker assumptions than the supervised topic model on the equal word feature distribution or parameter similarity.

Further, we believe that the supervised topic model may pose a logical paradox in terms of inference. Some approaches, e.g. Labeled-LDA (e.g., Ramage, Hall, Nallapati, and Manning 2009), incorporates the feature set ($`Y`$) to train the word compositions (the underlying construct of $`X`$). If such a result was generated and used to infer Y, the accuracy in terms of the label-set will be surely improved. However, the logic of inference under this circumstance can be questionable: Using the $`X`$ that are influenced by $`Y`$ to predict $`Y`$ seems to be spurious, for it requires an inference from $`Y`$ to predicted $`Y`$.  

## Data: Chinese Citizens Complaints to Provincial Government - An Example from Hebei Province

I use the proposed architecture to perform an illustrative task with political text data from China. Unlike the political texts used in the original paper, which were generated by German politicians, the texts here are written by citizens. This task can be viewed as a pressure test for the algorithm: Presumed that ordinary citizens are less professional in expressing political concerns, could this algorithm still capture some interesting insights?

The data were scrapped from the People's Daily Message Board [(人民网地方领导留言板)](http://liuyan.people.com.cn) in September 2018 (Code of web-scraping upon request). This platform is organized by the People's Daily, the mouthpiece of the Communist Party of China. Officials, usually Party Secretaries and Heads of Executive from provincial to county level are responsible for the reply. Here, I use the sample data from the Hebei Province (6000 observations), which is saved in `543.csv`. Important variables are first `zoom_1`, the texts written by citizens. The topic model will be generated from it. Second, `domainName`, the self-reported category of the complaint. Regarding the privacy concerns: These data are publicly available, and personal information such as names and IP-address are partially masked.

When citizens are submitting there complaints, they can choose a category that fits the content the most. There are in total 14 categories: Transportation (交通), Medicine (医疗), Urban planning (城建), Agriculture (三农), Education (教育), Bureaucracy (政务), Business (企业), Employment (就业), Environment (环保), Public security (治安), Finance (金融), Culture and entertainment (文娱), Tourism (旅游), *Other (其他)*. However, as external human coders would do, citizens may not choose the right category out of mistake, misunderstanding, or even on purpose (e.g., to enhance the seriousness of their concerns). Especially in terms of the "Other" category, the choice can be motivated by the multi-topical nature of the content.

Here, I am interested in the following question: __Based on the self-reported substantive categorization (i.e., other than "Other"-category), how can we infer the categorization of the texts labeled with the "Other"-category?__

## Results
__In particular, a topic model with 25 clusters and single-membership distribution fits the data (training and test) the best.__ For simple illustration (in English, automatically translated by Google Cloud Translate API), please refer to the average probability plot for topic models with [$`k = 25, \alpha = 2 `$](step_1/avg_prob_en/k25_a0_8.pdf) (Model 1) and with [$`k = 25, \alpha = 0.8 `$](step_1/avg_prob_en/k25_a0_8.pdf) (Model 2).

__In the second stage, I train a Multi-class SVM with substantively labeled texts (texts marked with a label other than "Other"). Model 1 achieved roughly 55% of the accuracy in the validation set, whereas Model 2 achieves 60%.__ For counteracting the overfit due to the imbalance between classes, I adjust the batch sample for stochastic gradient descent by overrepresenting small classes and underrepresenting the large ones ([__Batch SGD__ configuration sees here](step_2/SVM_reweight_info.csv)). Besides the training with only topical attributes, __I also include a time covariate - year dummy variables.__ With inclusion or even with interaction terms, the training accuracy enhances overall and also for multiple sub-classes. It indicates that year is not a relevant dimension. [The plot for validation accuracy against iterations can be found here](step_2/accuracy.pdf).

__Interestingly, I find that issues related to Agriculture (三农), Education (教育), Urban planning (城建), and Medicine (医疗) in particular return higher accuracy than the average.__ It seems that people using words relating to these issues have quite similar habits in terms of the word usage. For instance, even though urban planning covers many substantial fields, people who report this category tend to concentrate on real estates. The other three issues are rather particular in terms of word-using, therefore it is not a surprise that the classification task fits in so well.

If the goal were to boost the accuracy weight, what could be done is to overfit the model by enlarging the number of topical features or by using neural networks / adopting nonlinear kernels for the features. However, the __inference__ is a more interesting question here, since people may falsely report the categories motivated by many reasons. Specifically, the inference relates to the understanding of the data-generating process of human-generated labels. 

With regard to Model 2, I found two strong pieces of evidence:

- I generate an [average probability figure](step_2/avg_prob_en/k25_a0_8.pdf) of the test data by calculating the average topical feature probability regarding a particular label. __It suggests a reasonable match between the results of the unsupervised and supervised learning regarding most labels.__ Even classes like public security was not strongly identified by the topical features in the first step, texts with high probability with the topical feature *"Lv Zenong (An agriculture-service), Multi-level Marketing (A generally negatively perceived marketing strategy in China), settled down, household registration, certificates"* are generally labeled as public security issue. It is arguable that this topical feature shall appear in the "Bureaucracy" category, however, citizen's categorizing it with public security also makes sense.

- I plot the [SVM weights of the topical features](step_2/weight_plot_en/k25_a0_8.pdf) produced by the training set. __Again, clearly identified issues such as Agriculture, Education, Urban planning, and Medicine receive an unambiguous weight distribution.__ Bureaucracy, by contrast, is an encompassing concept where the citizens might be confused as well in choosing it.

Regarding the detailed results and discussions about them, please refer to the [report](report_latex/technical_report.pdf). This brief exercise suggests that this model does not only work with formal political text data but also travels to a different country with a different type of generating procedure of the labels. To gauge the generalizability of this architecture, however, needs future research in other different settings.

## Techinical Remarks, and How to Replicate

### Preprocessing of data

`corpora_creation.r` Unlike many other languages, there exists no space between different words in Chinese. I used the algorithm developed by the [jiebar Project](https://github.com/fxsjy/jieba) (Qin and Wu, 2018) to conduct the text segmentation in R. Based on the generated dictionary, I construct a text corpora and further a document-term matrix.

*Replication notes:* In running this code, please simply follow the instructions. For other types of segmentation, jiebaR-package itself also provides several other models. For a brief review about open-source applications regarding Chinese word segmentation, please see this [excellent review](https://blog.csdn.net/sinat_26917383/article/details/77067515) (in Chinese).

### First stage: Unsupervised learning - Generating topical attributes

`step_1_topicmodel.r` I use the recently developed [Structural Topic Model](https://www.structuraltopicmodel.com) (Roberts, Steward and Tingley, 2018) to realize the topic modeling. In essence, this algorithm builds upon the latent Dirichlet allocation (LDA) developed by Blei, Ng, and Jordan (2003), while allowing users to explicit model the priors that are related to the topical allocation and/or the loading of a word on a topical construct.

Dirichlet allocation is sensitive towards $`\alpha`$ and $`k`$. The former parameter indicates the peakedness of the distribution (single-membership or mixed-membership) and the latter the amount of potential topics. I perform the `stm::searchK()` function to find the best 38 parameter pairs based on the held-out likelihood (10% of the data) and semantic coherence (see Mimno, Wallach, Talley, Leenders, and McCallum, 2011). 

*Replication notes:* If users would like to perform the search and the selection based on other parameter-pairs, they can simply change `K` and `alpha` in the `selectK()` and `manyTopics` function. Note that I saved the models that are trained already in the repository, so users may directly import `readRDS()` to load the constructs for evaluation.

*---*

`step_1_topicmodel_eval.r` I picked 13 of them to conduct the second round of selection. I apply the `stm::manyTopics()` function to run 50 models for each paramter pairs for few iterations, and the better models (roughly 15%) are selected for a 100-iteration training (empirical convergence level: 1e-04 in terms of word bound). At last, with regard to each parameter pair, the model with the best performance in terms of the average topical exclusivity (Bischof and Airoldi 2012) and the semantic coherence will be saved. In the second round, I further reduce my modeling choices to only 2 models based on qualitative interpretation of the topical constructs ($`k = 25, \alpha = 0.8 `$ and $`k = 25, \alpha = 2 `$).

The generated point estimates of the posterior (calculated based on 1000 simulations through ``thetaPosterior()``) forms the *topical attributes*. The substantive meaning of these attributes are inferred by the top words, ranked by the frequency and exclusivity score (FREX) proposed by Bischof and Airoldi (2012). I report the relevance topical attributes for each of self-reported categories using the average probability of the texts in these topical attributes.

*Replication notes:* If users would like to change the rounds of simulations, please just refer to the parameter `nsim` in the function `thetaPosterior`. Also, users present the topical features in alternative ways by changing the weight parameter in the `frex_table()` function. I note that there are variety ways to label the topics, please refer to `stm::labelTopics()` for an overview.

The generation of the average probability plots can be also modified regarding the plotting parameters. In this study, I set the threshold of showing the topical features to 0.1, but it can also be set otherwise between 0 and 1. The English translation is powered by the [Google Cloud Translation API](https://cloud.google.com/translate/). By downloading the `.json` file that includes crucial information, users can use `gl_auth("../googlcl.json")` to process authentication. 

### Second stage: Supervised learning - Predicting uncategorized texts
`step_2_supervised.ipynb` With the supporting algorithms coded from scratch in `step_2_supervised_support.py`, I specify a multi-class support vector machine classification with batch stochastic gradient descent (linear kernel). The configuration is the following:

- Training data: 4923 observations with non-"Other" categories.
- Test data: 1077 observations with "Other" category.
- Feature set ($`X`$) specification: 
  - Only topical attributes ($`X_{topic}`$). Formally:
    ```math
      \hat{Y} = f_{svm}\left(a + X_{topic}\beta\right)
    ```
  - Topical attributes and covariates (year, $`YEAR`$):
    ```math
      \hat{Y} = f_{svm}\left(a + X_{topic}\beta + YEAR\theta\right)
    ```
  - An extended version with all possible interaction terms between topical attributes and covariates.
    
    ```math
      \hat{Y} = f_{svm}\left(a + X_{topic}\beta + YEAR\theta + \left(\sum^{||topic||}_{i = 1}\sum^{||year||}_{j = 1} X_{i}YEAR_{j}\right)\eta\right)
    ```

Based on the evaluation in the first stage, I selected two topic models with [$`k = 25, \alpha = 0.8 `$](step_1/avg_prob/k25_a0_8.pdf) (Model 1) and with [$`k = 25, \alpha = 2 `$](step_1/avg_prob/k25_a0_8.pdf) (Model 2) to perform the SVM. The specification of the batch sample can be seen [here](step_2/SVM_reweight_info.csv). Each model was run 4000 times. For detailed information on other parameters, please refer to the training file.

With regard to the trained models, I use the weights to predict the test data.

*Replication notes:* Please follow the comments of the code in `step_2_supervised_support.py` if users are interested in the technical details. The IPython provides a great interaction interface for users to immediately see the results in a markdown-style. In the end, all the models are saved in `.h5`-format with neccessary information, a hierarchical data organization that can be handled in multiple languages.

*---*

`step_2_supervised_eval.r` In this evaluation file, I provide several functions for the evaluation: `accuracy_plot()` automatically generates the history of validation accuracy of all variants based on a topical feature set. `metrics()` function provides a table with labels, precision, recall, and F1 measures for each class. The plotting of average probability plot follows the same logic as described in `step_1_topic_model_eval.r`. 

For evaluating prediction, I include the predicted labesl from all models into a dataset, and also compare their summary statistics. Regarding the degree of certainty regarding the prediction, I calculate the entropy across a (sub)set of models. 

Regarding the SVM-weights, I plot the point estimates given the label. I also provide a practice that automatically calculates the sum of interactive effects and topical effects given a year and a label.

*Replication notes:* This file has not fully commented yet. I am working on rendering some for-loops into apply-alike structure, as well as provide a better way to define the to-be-read models. However, users can generally follow the instructions. Regarding the Google Cloud Translation API, please consult their [official website](https://cloud.google.com/translate/). 



## Limitations and Future Research

__This research still has some unresolved limitations. First of all, the model selection in the first step - which is actually a more general caveat of topic models - can subject to fishing problems.__ In this exercise, I did not carry out a grid-search with many parameter-pairs. However, as readers can scrutinize the topic-words across the models being trained, there exist many linguistic similarities. Nevertheless, it is important to note that either the held-out likelihoods or the semantic coherence proposed by Mimno et al. (2011), cannot fully capture the "goodness" of the model. Future research might involve (1) n-gram extensions in the corpora-producing stage, and (2) exploiting the types of words using speech-tagging to produce more fine-grained topical loadings.

__Second, as all empirical research regarding the inference must acknowledge, the theoretical foundation needs to be justified through critical argumentations.__ In this exercise, I only include the year as the only covariate. However, this covariate might not be relevant, as suggested in the results. Here, we must seek political behavior/psychological foundations to find concepts that are of relevance, and further the appropriate measurements for it. __There is but also a technical side: Provided that the covariates are found - In what relation they are linking with the topical features? I.e., are they only affecting the generating-procedure of the labels ($`Y`$), or also the topical features ($`X_{topic}`$)?__ The latter part deserves a separate discussion in the future.

__Third, how shall the quality of the topical features to be interpreted?__ I chose the frex-algorithm to label the topical features. However, since they are produced by *clustering*, the topical features are not mutually exclusive of one and other even though they are treated as if they were discretely distributed in a Dirichlet allocation. The overlapping, in my view, shall be critically measured and shown with a mixture model (hierarchical model). Further, there exist some topical clusters that cannot be interpreted - Shall we discard them or not? Can it be viewed as an "error" cluster that might summarize "omitted topical clusters" altogether? Last but not least, as $`k`$ is shrinking, I observe some "absorption" of non-salient topical features into salient ones. This can be tested and solved when some high-quality labels exist; However, in a pure unsupervised learning scenario, it is sometimes hardly detectable.

## Related Literature
- Blei, David M., Andrew Y. Ng, and Michael I. Jordan (2003). "Latent dirichlet allocation." *Journal of Machine Learning Research* 3(Jan). pp: 993-1022.
- Qin, Wenfeng, and Yanyi Wu (2018). jiebaR: Chinese Text Segmentation. R package version 0.9.99. https://CRAN.R-project.org/package=jiebaR
- Roberts, Margaret E., Brandon M. Stewart, and Dustin Tingley (2018). stm: R Package for Structural Topic Models. http://www.structuraltopicmodel.com.
- Mimno, D., Wallach, H. M., Talley, E., Leenders, M., & McCallum, A. (2011, July). "Optimizing semantic coherence in topic models." In *Proceedings of the Conference on Empirical Methods in Natural Language Processing* (pp. 262-272). Association for Computational Linguistics. Chicago
- Bischof, Jonathan, and Edoardo M. Airoldi (2012). "Summarizing topical content with word frequency and exclusivity." *Proceedings of the 29th International Conference on Machine Learning (ICML-12)*.
- Ramage, D., Hall, D., Nallapati, R., & Manning, C. D. (2009). Labeled lda: A supervised topic model for credit attribution in multi-labeled corpora. In *Proceedings of the 2009 conference on empirical methods in natural language processing*: Volume 1-volume 1 (pp. 248–256).