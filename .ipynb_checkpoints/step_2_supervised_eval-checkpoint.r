library(rhdf5) # All supervised model's input and output are saved as h5 file
library(tidyverse)
library(plyr)
library(showtext)
library(googleLanguageR) # Google translation API

font_add("fangsong", regular = "AdobeFangsongStd-Regular.otf") # For showing Chinese texts


# Load data ---------------------------------------------------------------

label_info <- read_csv("step_1/label_set_info.csv") # one-to-one relationship between labels and their indicies

# Accuracy Plots ----------------------------------------------------------
#' Idea: Unpacking the accuracy history of different feature sets generated by all topic models

accuracy_plot <- function(model, metrics, ylim){
  
  #' Specifying the arguments

  if(metrics == 1){
    ylab_text = "L2 Loss"
  } else if (metrics == 2){
    ylab_text = "Training accuracy"
  } else if (metrics == 3){
    ylab_text = "Validation accuracy"
  } else {
    cat("Please provide either 1 (L2 Loss), 2 (Training accuracy) or 3  (Validation accuracy) or 4 (Overall accuracy) in the metric-argument")
  }
  
  if(model == "topic"){
    subtitle = "Only Topical Features"
  } else if(model == "incl_cov"){
    subtitle = "Topical Features with \n Additional Covariates (Year)"
  } else if(model == "all"){
    subtitle = "Topical Features, \n Additional Covariates, \n and their Interactions"
  } else {
    cat("Please provide either 'topic', 'incl_cov' or 'all' for the argument model. ")
  }
  
  #' Plotting an empty chart

  plot(c(1:250), type = "n", ylim=ylim,
       main = paste0(subtitle),
       ylab = ylab_text,
       xlab = "Epochs (per 200 Iterations)")
  
  #' For all selected topic models...

  for(i in c("step_2/k25_a0_8_", "step_2/k25_a2_")){
    path <- paste0(i, model,".h5")
    # model: "topic", "incl_cov", or "all"
    fid <- H5Fopen(path) # Read .h5 file
    temp <- h5dump(fid) # Using h5dump to transform it into a S3 object
    acc <- temp$acc_info$block0_values[metrics,] # Finding the type of accuracy
    acc <- acc[seq(0,length(acc),length.out = 201)] # Intervals
    lines(acc, col = "grey")
    lines(lowess(acc))
    overall_mean <- mean(as.vector(temp$train_label_set$block0_values) == 
                           as.vector(temp$train_pred_label$block0_values))
    
    text(200, mean(acc[100:200]), labels = paste0("Model ",str_remove_all(i, "step_2/|_"), ": ", round(overall_mean*100, 2)), col = "red")
    rm(temp)
  }
  
}

#' Plotting

pdf('step_2/accuracy.pdf', width = 12, height = 5)
par(mfrow=c(1,3))
accuracy_plot("topic", 3, c(0.4, 0.65))
accuracy_plot("incl_cov", 3, c(0.4, 0.65))
accuracy_plot("all", 3, c(0.4, 0.65))
par(mfrow=c(1,1))
dev.off()



# Accuracy table ----------------------------------------------------------

acc_types <- function(conf_matrix, class){
  # A function that compares each class
  tp <- conf_matrix[class, class] # Diagonal
  fp <- sum(conf_matrix[class, -class]) # Rows
  fn <- sum(conf_matrix[-class, class]) # Columns
  tn <- sum(conf_matrix[-class, -class]) # Other
  return(list("tp" = tp, "fp" = fp, "fn" = fn, "tn" = tn))
}

metrics <- function(k, alpha, feature){
  path <- paste0("step_2/k",k,"_a", str_replace(alpha, "[.]", "_"), "_",feature,".h5")
  fid <- H5Fopen(path)
  temp <- h5dump(fid)
  
  y <- temp$train_label_set$block0_values %>% as.vector()
  pred_y <- temp$train_pred_label$block0_values %>% as.vector()
  y <- as.factor(y)
  pred_y <- factor(pred_y, levels = levels(y))
  
  conf_matrix <- table(pred_y, y) %>% as.matrix()
  
  temp_table <- sapply(1:13, function(x) acc_types(conf_matrix, x))
  temp_table <- as_data_frame(t(temp_table)) %>% unnest()
  
  metrics_table <- temp_table %>% summarise(precision = tp/(tp+fp),
                                            recall = tp/(tp+fn),
                                            f1 = (2*precision*recall)/(precision + recall))
  metrics_table <- round(metrics_table*100, 2)
  metrics_table$label_num <- as.numeric(row.names(metrics_table)) - 1
  
  metrics_table <- inner_join(metrics_table, label_info)
  metrics_table <- metrics_table %>% select(label, precision, recall, f1, count) %>% mutate(prop = count/sum(count))
  
  return(metrics_table) 
}


k25_a0_8_topic_acc <- metrics(k = 25, alpha = 0.8, feature = "topic")
k25_a0_8_all_acc <- metrics(k = 25, alpha = 0.8, feature = "all")
k25_a2_topic_acc <- metrics(k = 25, alpha = 2, feature = "topic")

k25_a0_8_all_acc[,"f1_improve"] <- k25_a0_8_all_acc$f1-k25_a0_8_topic_acc$f1
library(xtable)
xtable(k25_a0_8_acc)
xtable(k25_a0_8_all_acc)


# Separate Analysis -------------------------------------------------------
#' Remark: Since there are only two models, users may just comment and un-comment
#' corresponding lines.

k = 25
#alpha = 0.8
alpha = 2

#type = "topic"
#type = "all"
type = "incl_cov"


path <- paste0("step_2/k",k,"_a", str_replace(alpha, "[.]", "_"), "_", type, ".h5")
fid <- H5Fopen(path)
temp <- h5dump(fid)

#' Frex
frex <- read.csv(paste0("step_1/frex/k",k,"_a", str_replace(alpha, "[.]", "_"),".csv"), stringsAsFactors = F)

gl_auth("../googlcl.json") # User must specify their own .json file. Please consult the Google Cloud Services and APIs

frex_en <- sapply(c(1:ncol(frex)), function(x) gl_translate(frex[1:10,x], target = "en", source = "zh-CN")$translatedText) # Only restrict to 10 top words




# Prediction analysis -----------------------------------------------------
#' Idea: Same as the average probability in the first stage, but this time
#' we apply it to the test data's (labeled with "Other") predicted labels
#' Comment on the codes please refer to step_1_topicmodel_eval.r

#' Prediction Distribution

pred_set <- table(temp$test_pred_label$block0_values %>% as.vector()) %>% as_data_frame()
pred_set$Var1 <- as.numeric(pred_set$Var1)
pred_set <- right_join(pred_set, label_info, by = c("Var1" = "label_num"))
pred_set <- pred_set %>% mutate(prop = round((n/1077)*100, 2) )
pred_set <- pred_set %>% select(label, n, prop)

xtable(pred_set)


#' Average Probability
test_feature <- as_data_frame(t(temp$test_set$block0_values))[,-1] 
test_feature <- test_feature/100

test_label_set <- temp$test_pred_label$block0_values %>% as.vector()
test_label_set <- plyr::mapvalues(test_label_set, label_info$label_num, label_info$label)

threshold = 0.1

pdf(paste0("step_2/avg_prob/","k",k, "_", "a", str_replace(alpha, "[.]", "_"),"_", type, ".pdf"), width = 8.27, height = 11.69)

par(mfrow = c(3,2))

for(i in 1:length(unique(test_label_set))){
  ind_sub <- which(test_label_set == (unique(test_label_set)[i]))
  
  test_feature_sub <- test_feature[ind_sub,1:25]
  test_feature_sub <- colMeans(test_feature_sub)
  
  showtext_begin()
  
  plot(test_feature_sub, type = "p", ylim = c(0,1), xlim = c(1,k+1),
       pch = 19,
       main = paste("Label - ", unique(test_label_set)[i]),
       ylab = "Probability",
       xlab = paste("Topic model with k =", k, "alpha =", alpha, "(Threshold for showing = ", threshold, ")"))
  
  
  topics_d <- which(test_feature_sub > threshold)
  
  if(length(topics_d) > 0){
    sapply(topics_d, function(x) text(x+1, test_feature_sub[x],
                                      str_c(frex[1:5,x], collapse = "\n")))
  }
}

dev.off()

  #' English version

pdf(paste0("step_2/avg_prob_en/","k",k, "_", "a", str_replace(alpha, "[.]", "_"),"_", type, ".pdf"), width = 8.27, height = 11.69)

par(mfrow = c(3,2))

for(i in 1:length(unique(test_label_set))){
  ind_sub <- which(test_label_set == (unique(test_label_set)[i]))
  
  test_feature_sub <- test_feature[ind_sub,1:25]
  test_feature_sub <- colMeans(test_feature_sub)
  
  showtext_begin()
  
  plot(test_feature_sub, type = "p", ylim = c(0,1), xlim = c(1,k+1),
       pch = 19,
       main = paste("Label - ", unique(test_label_set)[i]),
       ylab = "Probability",
       xlab = paste("Topic model with k =", k, "alpha =", alpha, "(Threshold for showing = ", threshold, ")"))
  
  
  topics_d <- which(test_feature_sub > threshold)
  
  if(length(topics_d) > 0){
    sapply(topics_d, function(x) text(x+1, test_feature_sub[x],
                                      str_c(frex_en[1:5,x], collapse = "\n")))
  }
}

dev.off()

# Prediction and Entropy --------------------------------------------------

models <- list.files("step_2")[str_detect(list.files("step_2"),".h5")]

pred_all_mod <- label_info[,1:2]
all_pred <- data_frame(doc = c(1:1077))

for(i in 1:length(models)){
  
  path <- paste0("step_2/",models[i])
  fid <- H5Fopen(path)
  temp <- h5dump(fid)
  
  pred_set <- table(temp$test_pred_label$block0_values %>% as.vector()) %>% as_data_frame()
  pred_set$Var1 <- as.numeric(pred_set$Var1)
  names(pred_set) <- c("label_num", models[i])
  pred_all_mod <- left_join(pred_all_mod, pred_set)
  
  all_pred <- cbind(all_pred, unlist(as.vector(temp$test_pred_label$block0_values)))
  names(all_pred)[ncol(all_pred)] <- models[i]
}

pred_all_mod[,c(1,3:5)] %>% xtable()
all_pred <- as.data.frame(all_pred)


library(entropy)
all_pred$entropy_0_8 <- sapply(1:nrow(all_pred), function(x) entropy(table(unlist(all_pred[x,2:4]))))
all_pred$entropy_2 <- sapply(1:nrow(all_pred), function(x) entropy(table(unlist(all_pred[x,5:7]))))
all_pred$entropy_all <- sapply(1:nrow(all_pred), function(x) entropy(table(unlist(all_pred[x,2:7]))))

write.csv(all_pred, "step_2/all_pred_with_entropy.csv")

# Tourism?
prov1_test <- prov1[prov1$domainName == "其他",]
pred_tourism <- prov1_test[unique(c(which(all_pred$k25_a0_8_topic.h5 == 9), which(all_pred$k25_a0_8_incl_cov.h5 == 9), which(all_pred$k25_a0_8_all.h5 == 9))), "zoom_1"]

pred_tourism_en <- gl_translate(unlist(pred_tourism), target = "en", source = "zh-CN")$translatedText

# Weight analysis ---------------------------------------------------------
#' Idea: Plotting weights of the topical features for a given label

threshold = 0.01

pdf(paste0("step_2/weight_plot/","k",k, "_", "a", str_replace(alpha, "[.]", "_"),".pdf"), width = 17, height = 9)

par(mfrow = c(2,4))

for(i in 1:13){
  #' Extracting weights. Excluding the first column since they are estimates of the bias term (column with 1s).

  weights <- temp$best_W$block0_values[i,2:26]
  
  showtext_begin()
  
  #' Plotting weights
  plot(x = weights, y = c(1:25), xlim = c(-0.01, 0.05), ylim = c(0,30), axes = F,
       xlab = "", ylab = "",
       pch = 19, col = "grey")
  axis(1)
  axis(2, at = c(1:25))
  
  #' Plotting titles
  title(main = paste0("Label - ",label_info$label[i]), 
        xlab = paste0("Weights with k =", k, "alpha =", alpha, "\n (Threshold for showing = ", threshold, ")"),
        ylab = "Topical Feature")
  
  abline(v=0)
  
  topic_attribute_ind <- which(weights > threshold)
  
  #' Adding texts
  sapply(topic_attribute_ind, function(x) text(weights[x], x,
                                               str_wrap(str_c(frex[1:5,x], collapse = ", "), width = 20)))
}

dev.off()

pdf(paste0("step_2/weight_plot_en/","k",k, "_", "a", str_replace(alpha, "[.]", "_"),".pdf"), width = 17, height = 9)

par(mfrow = c(2,4))

for(i in 1:13){
  weights <- temp$best_W$block0_values[i,2:26]
  
  showtext_begin()
  
  plot(x = weights, y = c(1:25), xlim = c(-0.01, 0.05), ylim = c(0,30), axes = F,
       xlab = "", ylab = "",
       pch = 19, col = "grey")
  axis(1)
  axis(2, at = c(1:25))
  
  title(main = paste0("Label - ",label_info$label[i]), 
        xlab = paste0("Weights with k =", k, "alpha =", alpha, "\n (Threshold for showing = ", threshold, ")"),
        ylab = "Topical Feature")
  
  abline(v=0)
  
  topic_attribute_ind <- which(weights > threshold)
  
  sapply(topic_attribute_ind, function(x) text(weights[x], x,
                                               str_wrap(str_c(frex_en[1:5,x], collapse = ", "), width = 20)))
}

dev.off()


# Analysis with covariates ------------------------------------------------
# k25_a_0_8

path <- paste0("step_2/k25_a0_8_all",".h5")
fid <- H5Fopen(path)
temp <- h5dump(fid)

# Information on covariates: Appended in the last five columns
# 2014, 2015, 2016, 2017, 2018
temp$train_set$block0_values[27:31,] %>% t() %>% head()

temp$train_set$block0_values[27:37,] %>% t() %>% head()

year_cov <- temp$best_W$block0_values[,27:31]
i = 5

par(mfrow = c(2,3))

for(i in 1:5){
  plot(x = year_cov[,i], y = c(1:13), xlim = c(-0.005, 0.005), ylim = c(1,13),
       pch = 20, col = "grey", main = paste0(2013+i),
       xlab = "Weight", ylab = "Label")
  abline(v=0)
  
  for(l in 1:13){
    if(year_cov[l,i] > 0){
      points(year_cov[l,i], label_info$label_num[l]+1, col = "grey50", pch = 19)
      text(year_cov[l,i], label_info$label_num[l]+1, label_info$label[l], pos = 4)
    }
  }
  
}

par(mfrow = c(1,1))


# Interaction effect
# In each year, the year_pos of a five-interval
int_term <- temp$best_W$block0_values[,32:156]
topic_term <- temp$best_W$block0_values[,2:26]

y <- 5

a <- rep(0,13)
for(i in 1:25){
  a <- cbind(a, topic_term[,i] + int_term[, (i-1)*5 + y])
}

a <- a[,-1]


i <- 4

#for(i in 1:13){
  weights <- a[i,]
  
  showtext_begin()
  
  plot(x = weights, y = c(1:25), xlim = c(-0.01, 0.05), ylim = c(0,30), axes = F,
       xlab = "", ylab = "",
       pch = 19, col = "grey")
  axis(1)
  axis(2, at = c(1:25))
  
  title(main = paste0("Label - ",label_info$label[i]), 
        xlab = paste0("Weights with k =", k, "alpha =", alpha, "\n (Threshold for showing = ", threshold, ")"),
        ylab = "Topical Feature")
  
  abline(v=0)
  
  topic_attribute_ind <- which(weights > threshold)
  
  sapply(topic_attribute_ind, function(x) text(weights[x], x,
                                               str_wrap(str_c(frex_en[1:5,x], collapse = ", "), width = 20)))
#}


year_plot <- function(issue, frex_vers){
  
  for(y in 1:5){
    y <- y
    
    a <- rep(0,13)
    for(i in 1:25){
      a <- cbind(a, topic_term[,i] + int_term[, (i-1)*5 + y])
    }
    
    a <- a[,-1]
    
    i = issue
    weights <- a[i,]
    
    showtext_begin()
    
    plot(x = weights, y = c(1:25), xlim = c(-0.01, 0.05), ylim = c(0,30), axes = F,
         xlab = "", ylab = "",
         pch = 19, col = "grey")
    axis(1)
    axis(2, at = c(1:25))
    
    title(main = paste0("Label - ",label_info$label[i], " (",y+2013,")"), 
          xlab = paste0("Weights with k =", k, "alpha =", alpha, "\n (Threshold for showing = ", threshold, ")"),
          ylab = "Topical Feature")
    
    abline(v=0)
    
    topic_attribute_ind <- which(weights > threshold)
    
    frex_vers <- frex_vers
    
    sapply(topic_attribute_ind, function(x) text(weights[x], x,
                                                 str_wrap(str_c(frex_vers[1:5,x], collapse = ", "), width = 20)))
  }
}




for(i in 1:13){
  pdf(paste0("step_2/weight_plot_year/",label_info$label[i],".pdf"), width = 15, height = 7)
  par(mfrow = c(2,3))
  year_plot(i, frex)
  par(mfrow = c(1,1))
  dev.off()
}

for(i in 1:13){
  pdf(paste0("step_2/weight_plot_year_en/",label_info$label[i],".pdf"), width = 15, height = 7)
  par(mfrow = c(2,3))
  year_plot(i, frex_en)
  par(mfrow = c(1,1))
  dev.off()
}